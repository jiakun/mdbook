<link rel="stylesheet" type="text/css" href="style/mystyle.css">
<div id="top"></div>

<a href="#html5api">Html5 API</a><br />
<a href="#reference">Reference</a><br />
<a href="#htmlelement">Html Element</a><br />
<a href="#CSS">CSS</a>

<div id="html5api"></div>

<details open>
<summary style="font-size: 60px">Html5 API</summary>

<a href="#geolocation">geolocation</a><br />
<a href="#draggable">draggable</a><br />
<a href="#storage">Storage</a><br />
<a href="#webworker">Web Worker</a><br />
<a href="#sse">SSE</a><br />
<a href="#htmlcachemanifest">HTML Cache Manifest</a><br />
<a href="#websocket">WebSocket</a><br />

<div id="geolocation"></div><br />
<a href="#html5api">top</a><br />
<a href="#top">pagetop</a>

# Geolocation

```html
<button onclick="getGeo()">geolocations</button>
<script>
    function getGeo () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(success, error)
        } else {
            console.log("your browser not support geolocation !!")
        }
    }
    function error (err) {
        console.log(err)
    }
    function success (position) {
        console.log("latitude: "+ position.coords.latitude)
        console.log("longitude: "+ position.coords.longitude)
    }
</script>
```

<button onclick="getGeo()">geolocations</button>
<script>
    function getGeo () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(success, error)
        } else {
            console.log("your browser not support geolocation !!")
        }
    }
    function error (err) {
        console.log(err)
    }
    function success (position) {
        console.log("latitude: "+ position.coords.latitude)
        console.log("longitude: "+ position.coords.longitude)
    }
</script>

<div id="draggable"></div><br />
<a href="#html5api">top</a><br />
<a href="#top">pagetop</a>

# draggable
```html
<script type="text/javascript">
    function allowDrop(ev)
    {
        ev.preventDefault();
    }

    function drag(ev)
    {
        ev.dataTransfer.setData("Text",ev.target.id);
    }

    function drop(ev)
    {
        var data=ev.dataTransfer.getData("Text");
        ev.target.appendChild(document.getElementById(data));
        ev.preventDefault();
    }
</script>
<div id="div1" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
<br />
<p id="drag1" draggable="true" ondragstart="drag(event)">这是一段可移动的段落。请把该段落拖入上面的矩形。</p>
```
<script type="text/javascript">
        function allowDrop(ev)
        {
            ev.preventDefault();
        }

        function drag(ev)
        {
            ev.dataTransfer.setData("Text",ev.target.id);
        }

        function drop(ev)
        {
            var data=ev.dataTransfer.getData("Text");
            ev.target.appendChild(document.getElementById(data));
            ev.preventDefault();
        }
</script>
<div id="div1" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
<br />
<p id="drag1" draggable="true" ondragstart="drag(event)">这是一段可移动的段落。请把该段落拖入上面的矩形。</p>

<div id="storage"></div><br />
<a href="#html5api">top</a><br />
<a href="#top">pagetop</a>

# Storage
<img src="example/html5/img/localstorage-sessionstorage.png" />

```html
    <div id="name"></div>
    <div id="hello"></div>
    <script>
        if (typeof(Storage) !== "undefined") {
            localStorage.hello = "world"
            localStorage.setItem("name", "jiakunguo")
            sessionStorage.hello = "world"
            sessionStorage.setItem("name", "jiakunguo")
            document.getElementById("hello").innerHtml = localStorage.getItem("hello")
            document.getElementById("name").innerHtml = localStorage.name
            localStorage.removeItem("name")
            localStorage.clear()
        } else {
            window.alert(" your browser not support Storage !!")
        }
    </script>
```
<script>
        localStorage.hello = "world"
        localStorage.setItem("name", "jiakunguo")
        sessionStorage.hello = "world"
        sessionStorage.setItem("name", "jiakunguo")
</script>

<div id="webworker"></div><br />
<a href="#html5api">top</a><br />
<a href="#top">pagetop</a>

# Web Worker
```html
    <div id="result"></div>
    <script>
        var w
        if (typeof(Worker) !== "undefined") {
            w = new Worker("example/html5/demo_worker.js")
            w.onmessage = function(event){
                document.getElementById("result").innerHTML = event.data
            }
        } else {
            console.log("your browser not support web Worker")
        }
    </script>
```
<div id="result"></div>
<script>
    var w
    if (typeof(Worker) !== "undefined") {
       w = new Worker("example/html5/demo_worker.js")
        w.onmessage = function(event){
            document.getElementById("result").innerHTML = event.data
        }
    } else {
        console.log("your browser not support web Worker")
    }
</script>

<div id="sse"></div><br />
<a href="#html5api">top</a><br />
<a href="#top">pagetop</a>

# SSE  <sub>Server Sent Event - One way messageing</sub>

```html
    <script>
        if (typeof(EventSource) !== "undefined") {
            var sse = new EventSource("https://mock-js.herokuapp.com/others/sse")
            sse.onmessage = function (event) {
                console.log(event.data)
            }
        } else {
            console.log(" not support event source")
        }
    </script>
    <!-- on node express server -->
    app.get('/sse', function (request, response) {
        response.status(200).set({
        "connection": "keep-alive",
        "cache-control": "no-cache",
        "content-Type": "text/event-stream"
    })

        response.write(`data: Hello SSE !!\n\n`)
    })
```
<script>
        if (typeof(EventSource) !== "undefined") {
            var sse = new EventSource("https://mock-js.herokuapp.com/others/sse")
            sse.onmessage = function (event) {
                console.log(event.data)
            }
        } else {
            console.log(" not support event source")
        }
</script>
<div>
    <p>在上例中，我们使用 onmessage 事件来获取消息。不过还可以使用其他事件：</p>
    <table class="dataintable">
        <tbody><tr>
            <th>事件</th>
            <th>描述</th>
        </tr>
        <tr>
            <td>onopen</td>
            <td>当通往服务器的连接被打开</td>
        </tr>
        <tr>
            <td>onmessage</td>
            <td>当接收到消息</td>
        </tr>
        <tr>
            <td>onerror</td>
            <td>当发生错误</td>
        </tr>
        </tbody></table>
</div>

<div id="htmlcachemanifest"></div><br />
<a href="#html5api">top</a><br />
<a href="#top">pagetop</a>

# HTML Cache Manifest <sup>deprecated in chrome79 </sup>  
<sup>https://www.chromestatus.com/feature/6192449487634432</sup>
```html
<!DOCTYPE HTML>
<html manifest="demo.appcache">

<body>
文档内容 ......
</body>

</html>

<!-- demo.appcache -->
CACHE MANIFEST
# v1.0.0
/theme.css
/logo.gif
/main.js

NETWORK:
login.asp //or *

FALLBACK:
/html/ /offline.html // offline.html replace html dir all file

```

<div id="websocket"></div><br />
<a href="#html5api">top</a><br />
<a href="#top">pagetop</a>

# WebSocket
```html
<script>
if (typeof(WebSocket !== 'undefine')) {
  var ws = new WebSocket('wss://mock-js.herokuapp.com')
  ws.onopen = function () {
    ws.send('hello server')
  }
  ws.onmessage = function (evt) {
    console.log("ws-> message from server: "+ evt.data)
    ws.close()
  }

  ws.onclose = function () {
    console.log('ws close')
  }
} else {
  console.log('your browser not support websocket')
}
</script>
<!-- on nodejs express server-->

var websocket = require('ws')
class WS {
    constructor (server) {
        this.server = server
    }
    handle() {
        var server = this.server
        var socketio = new websocket.Server({server})
        socketio.on('connection', async (socket)=> {
            console.log('a user connection to server')
            socket.on('message', (obj)=> {
                console.log('ws->frome clinet message: '+ obj)
                socket.send('hello client')
            })

            socket.on('close', ()=> {
                console.log('disconnect from client')
            })
        })
    }
}

module.exports = WS
```
<script>
if (typeof(WebSocket !== 'undefine')) {
  var ws = new WebSocket('wss://mock-js.herokuapp.com')
  ws.onopen = function () {
    ws.send('hello server')
  }
  ws.onmessage = function (evt) {
    console.log("ws-> message from server: "+ evt.data)
    ws.close()
  }

  ws.onclose = function () {
    console.log('ws close')
  }
} else {
  console.log('your browser not support websocket')
}
</script>
[https://github.com/websockets/ws#simple-server](https://github.com/websockets/ws#simple-server)

[https://www.npmjs.com/package/express-ws](https://www.npmjs.com/package/express-ws)

[https://socket.io/get-started/chat/#Introduction](https://socket.io/get-started/chat/#Introduction) & [https://github.com/socketio/socket.io-client](https://github.com/socketio/socket.io-client)
</details>


<div id="reference"></div>
<a href="#top">pagetop</a>



<details open>
<summary style="font-size: 60px">Reference</summary>

<a href="#eventproperty">EventProperty</a><br />
<a href="#urlcode">urlcode</a><br />
<a href="#httpmessage">httpmessage</a><br />
<a href="#httpmethod">httpmethod</a><br />
<a href="#globalproperty">globalproperty</a><br />


<div id="eventproperty"></div><br />
<a href="#reference">top</a><br />
<a href="#top">pagetop</a>

<details open>
<summary>EventProperty</summary>

<div>
<h2>Window 事件属性</h2>

<p>针对 window 对象触发的事件（应用到 &lt;body&gt; 标签）：</p>

<table class="dataintable">
<tbody><tr>
<th style="width:27%;">属性</th>
<th style="width:8%;">值</th>
<th>描述</th>
</tr>

<tr>
<td class="html5_new"><a href="/tags/event_onafterprint.asp" title="HTML onafterprint 事件属性">onafterprint</a></td>
<td>script</td>
<td>文档打印之后运行的脚本。</td>
</tr>

<tr>
<td class="html5_new"><a href="/tags/event_onbeforeprint.asp" title="HTML onbeforeprint 事件属性">onbeforeprint</a></td>
<td>script</td>
<td>文档打印之前运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onbeforeunload</td>
<td>script</td>
<td>文档卸载之前运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onerror</td>
<td>script</td>
<td>在错误发生时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onhaschange</td>
<td>script</td>
<td>当文档已改变时运行的脚本。</td>
</tr>

<tr>
<td><a href="/tags/event_onload.asp" title="HTML onload 事件属性">onload</a></td>
<td>script</td>
<td>页面结束加载之后触发。</td>
</tr>

<tr>
<td class="html5_new">onmessage</td>
<td>script</td>
<td>在消息被触发时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onoffline</td>
<td>script</td>
<td>当文档离线时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">ononline</td>
<td>script</td>
<td>当文档上线时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onpagehide</td>
<td>script</td>
<td>当窗口隐藏时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onpageshow</td>
<td>script</td>
<td>当窗口成为可见时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onpopstate</td>
<td>script</td>
<td>当窗口历史记录改变时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onredo</td>
<td>script</td>
<td>当文档执行撤销（redo）时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new"><a href="/tags/event_onresize.asp" title="HTML onresize 事件属性">onresize</a></td>
<td>script</td>
<td>当浏览器窗口被调整大小时触发。</td>
</tr>

<tr>
<td class="html5_new">onstorage</td>
<td>script</td>
<td>在 Web Storage 区域更新后运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onundo</td>
<td>script</td>
<td>在文档执行 undo 时运行的脚本。</td>
</tr>

<tr>
<td><a href="/tags/event_onunload.asp" title="HTML onunload 事件属性">onunload</a></td>
<td>script</td>
<td>一旦页面已下载时触发（或者浏览器窗口已被关闭）。</td>
</tr>
</tbody></table>
</div>

<div>
<h2>Form 事件</h2>

<p>由 HTML 表单内的动作触发的事件（应用到几乎所有 HTML 元素，但最常用在 form 元素中）：</p>

<table class="dataintable">
<tbody><tr>
<th style="width:27%;">属性</th>
<th style="width:8%;">值</th>
<th>描述</th>
</tr>

<tr>
<td><a href="/tags/event_onblur.asp" title="HTML onblur 事件属性">onblur</a></td>
<td>script</td>
<td>元素失去焦点时运行的脚本。</td>
</tr>

<tr>
<td><a href="/tags/event_onchange.asp" title="HTML onchange 事件属性">onchange</a></td>
<td>script</td>
<td>在元素值被改变时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">oncontextmenu</td>
<td>script</td>
<td>当上下文菜单被触发时运行的脚本。</td>
</tr>

<tr>
<td><a href="/tags/event_onfocus.asp" title="HTML onfocus 事件属性">onfocus</a></td>
<td>script</td>
<td>当元素获得焦点时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onformchange</td>
<td>script</td>
<td>在表单改变时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onforminput</td>
<td>script</td>
<td>当表单获得用户输入时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">oninput</td>
<td>script</td>
<td>当元素获得用户输入时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">oninvalid</td>
<td>script</td>
<td>当元素无效时运行的脚本。</td>
</tr>

<tr>
<td>onreset</td>
<td>script</td>
<td>当表单中的重置按钮被点击时触发。HTML5 中不支持。</td>
</tr>

<tr>
<td><a href="/tags/event_onselect.asp" title="HTML onselect 事件属性">onselect</a></td>
<td>script</td>
<td>在元素中文本被选中后触发。</td>
</tr>

<tr>
<td><a href="/tags/event_onsubmit.asp" title="HTML onsubmit 事件属性">onsubmit</a></td>
<td>script</td>
<td>在提交表单时触发。</td>
</tr>
</tbody></table>
</div>

<div>
<h2>Keyboard 事件</h2>

<table class="dataintable">
<tbody><tr>
<th style="width:27%;">属性</th>
<th style="width:8%;">值</th>
<th>描述</th>
</tr>

<tr>
<td><a href="/tags/event_onkeydown.asp" title="HTML onkeydown 事件属性">onkeydown</a></td>
<td>script</td>
<td>在用户按下按键时触发。</td>
</tr>

<tr>
<td><a href="/tags/event_onkeypress.asp" title="HTML onkeypress 事件属性">onkeypress</a></td>
<td>script</td>
<td>在用户敲击按钮时触发。</td>
</tr>

<tr>
<td><a href="/tags/event_onkeyup.asp" title="HTML onkeyup 事件属性">onkeyup</a></td>
<td>script</td>
<td>当用户释放按键时触发。</td>
</tr>
</tbody></table>
</div>

<div>
<h2>Mouse 事件</h2>

<p>由鼠标或类似用户动作触发的事件：</p>

<table class="dataintable">
<tbody><tr>
<th style="width:27%;">属性</th>
<th style="width:8%;">值</th>
<th>描述</th>
</tr>

<tr>
<td><a href="/tags/event_onclick.asp" title="HTML onclick 事件属性">onclick</a></td>
<td>script</td>
<td>元素上发生鼠标点击时触发。</td>
</tr>

<tr>
<td><a href="/tags/event_ondblclick.asp" title="HTML ondblclick 事件属性">ondblclick</a></td>
<td>script</td>
<td>元素上发生鼠标双击时触发。</td>
</tr>

<tr>
<td class="html5_new">ondrag</td>
<td>script</td>
<td>元素被拖动时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">ondragend</td>
<td>script</td>
<td>在拖动操作末端运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">ondragenter</td>
<td>script</td>
<td>当元素元素已被拖动到有效拖放区域时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">ondragleave</td>
<td>script</td>
<td>当元素离开有效拖放目标时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">ondragover</td>
<td>script</td>
<td>当元素在有效拖放目标上正在被拖动时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">ondragstart</td>
<td>script</td>
<td>在拖动操作开端运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">ondrop</td>
<td>script</td>
<td>当被拖元素正在被拖放时运行的脚本。</td>
</tr>

<tr>
<td><a href="/tags/event_onmousedown.asp" title="HTML onmousedown 事件属性">onmousedown</a></td>
<td>script</td>
<td>当元素上按下鼠标按钮时触发。</td>
</tr>

<tr>
<td><a href="/tags/event_onmousemove.asp" title="HTML onmousemove 事件属性">onmousemove</a></td>
<td>script</td>
<td>当鼠标指针移动到元素上时触发。</td>
</tr>

<tr>
<td><a href="/tags/event_onmouseout.asp" title="HTML onmouseout 事件属性">onmouseout</a></td>
<td>script</td>
<td>当鼠标指针移出元素时触发。</td>
</tr>

<tr>
<td><a href="/tags/event_onmouseover.asp" title="HTML onmouseover 事件属性">onmouseover</a></td>
<td>script</td>
<td>当鼠标指针移动到元素上时触发。</td>
</tr>

<tr>
<td><a href="/tags/event_onmouseup.asp" title="HTML onmouseup 事件属性">onmouseup</a></td>
<td>script</td>
<td>当在元素上释放鼠标按钮时触发。</td>
</tr>

<tr>
<td class="html5_new">onmousewheel</td>
<td>script</td>
<td>当鼠标滚轮正在被滚动时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onscroll</td>
<td>script</td>
<td>当元素滚动条被滚动时运行的脚本。</td>
</tr>
</tbody></table>
</div>

<div>
<h2>Media 事件</h2>

<p>由媒介（比如视频、图像和音频）触发的事件（适用于所有 HTML 元素，但常见于媒介元素中，比如 &lt;audio&gt;、&lt;embed&gt;、&lt;img&gt;、&lt;object&gt; 以及 &lt;video&gt;）:</p>

<table class="dataintable">
<tbody><tr>
<th style="width:27%;">属性</th>
<th style="width:8%;">值</th>
<th>描述</th>
</tr>

<tr>
<td>onabort</td>
<td>script</td>
<td>在退出时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">oncanplay</td>
<td>script</td>
<td>当文件就绪可以开始播放时运行的脚本（缓冲已足够开始时）。</td>
</tr>

<tr>
<td class="html5_new">oncanplaythrough</td>
<td>script</td>
<td>当媒介能够无需因缓冲而停止即可播放至结尾时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">ondurationchange</td>
<td>script</td>
<td>当媒介长度改变时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onemptied</td>
<td>script</td>
<td>当发生故障并且文件突然不可用时运行的脚本（比如连接意外断开时）。</td>
</tr>

<tr>
<td class="html5_new">onended</td>
<td>script</td>
<td>当媒介已到达结尾时运行的脚本（可发送类似“感谢观看”之类的消息）。</td>
</tr>

<tr>
<td class="html5_new">onerror</td>
<td>script</td>
<td>当在文件加载期间发生错误时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onloadeddata</td>
<td>script</td>
<td>当媒介数据已加载时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onloadedmetadata</td>
<td>script</td>
<td>当元数据（比如分辨率和时长）被加载时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onloadstart</td>
<td>script</td>
<td>在文件开始加载且未实际加载任何数据前运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onpause</td>
<td>script</td>
<td>当媒介被用户或程序暂停时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onplay</td>
<td>script</td>
<td>当媒介已就绪可以开始播放时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onplaying</td>
<td>script</td>
<td>当媒介已开始播放时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onprogress</td>
<td>script</td>
<td>当浏览器正在获取媒介数据时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onratechange</td>
<td>script</td>
<td>每当回放速率改变时运行的脚本（比如当用户切换到慢动作或快进模式）。</td>
</tr>

<tr>
<td class="html5_new">onreadystatechange</td>
<td>script</td>
<td>每当就绪状态改变时运行的脚本（就绪状态监测媒介数据的状态）。</td>
</tr>

<tr>
<td class="html5_new">onseeked</td>
<td>script</td>
<td>当 seeking 属性设置为 false（指示定位已结束）时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onseeking</td>
<td>script</td>
<td>当 seeking 属性设置为 true（指示定位是活动的）时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onstalled</td>
<td>script</td>
<td>在浏览器不论何种原因未能取回媒介数据时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onsuspend</td>
<td>script</td>
<td>在媒介数据完全加载之前不论何种原因终止取回媒介数据时运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">ontimeupdate</td>
<td>script</td>
<td>当播放位置改变时（比如当用户快进到媒介中一个不同的位置时）运行的脚本。</td>
</tr>

<tr>
<td class="html5_new">onvolumechange</td>
<td>script</td>
<td>每当音量改变时（包括将音量设置为静音）时运行的脚本。</td>
</tr>

<tr>
<td>onwaiting</td>
<td>script</td>
<td>当媒介已停止播放但打算继续播放时（比如当媒介暂停已缓冲更多数据）运行脚本</td>
</tr>
</tbody></table>
</div>

</details>

<div id="urlcode"></div><br />
<a href="#reference">top</a><br />
<a href="#top">pagetop</a>

<details open>
<summary>URL Code</summary>

<table class="dataintable">
    <tbody><tr>
      <th>ASCII Value</th>
      <th>URL-encode</th>
      <th>ASCII Value</th>
      <th>URL-encode</th>
      <th>ASCII Value</th>
      <th>URL-encode</th>
      </tr>
    <tr>
      <td>æ</td>
      <td>%00</td>
      <td>0</td>
      <td>%30</td>
      <td>`</td>
      <td>%60</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%01</td>
      <td>1</td>
      <td>%31</td>
      <td>a</td>
      <td>%61</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%02</td>
      <td>2</td>
      <td>%32</td>
      <td>b</td>
      <td>%62</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%03</td>
      <td>3</td>
      <td>%33</td>
      <td>c</td>
      <td>%63</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%04</td>
      <td>4</td>
      <td>%34</td>
      <td>d</td>
      <td>%64</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%05</td>
      <td>5</td>
      <td>%35</td>
      <td>e</td>
      <td>%65</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%06</td>
      <td>6</td>
      <td>%36</td>
      <td>f</td>
      <td>%66</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%07</td>
      <td>7</td>
      <td>%37</td>
      <td>g</td>
      <td>%67</td>
    </tr>
    <tr>
      <td>backspace</td>
      <td>%08</td>
      <td>8</td>
      <td>%38</td>
      <td>h</td>
      <td>%68</td>
    </tr>
    <tr>
      <td>tab</td>
      <td>%09</td>
      <td>9</td>
      <td>%39</td>
      <td>i</td>
      <td>%69</td>
    </tr>
    <tr>
      <td>linefeed</td>
      <td>%0a</td>
      <td>:</td>
      <td>%3a</td>
      <td>j</td>
      <td>%6a</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%0b</td>
      <td>;</td>
      <td>%3b</td>
      <td>k</td>
      <td>%6b</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%0c</td>
      <td>&lt;</td>
      <td>%3c</td>
      <td>l</td>
      <td>%6c</td>
    </tr>
    <tr>
      <td>c return</td>
      <td>%0d</td>
      <td>=</td>
      <td>%3d</td>
      <td>m</td>
      <td>%6d</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%0e</td>
      <td>&gt;</td>
      <td>%3e</td>
      <td>n</td>
      <td>%6e</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%0f</td>
      <td>?</td>
      <td>%3f</td>
      <td>o</td>
      <td>%6f</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%10</td>
      <td>@</td>
      <td>%40</td>
      <td>p</td>
      <td>%70</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%11</td>
      <td>A</td>
      <td>%41</td>
      <td>q</td>
      <td>%71</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%12</td>
      <td>B</td>
      <td>%42</td>
      <td>r</td>
      <td>%72</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%13</td>
      <td>C</td>
      <td>%43</td>
      <td>s</td>
      <td>%73</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%14</td>
      <td>D</td>
      <td>%44</td>
      <td>t</td>
      <td>%74</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%15</td>
      <td>E</td>
      <td>%45</td>
      <td>u</td>
      <td>%75</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%16</td>
      <td>F</td>
      <td>%46</td>
      <td>v</td>
      <td>%76</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%17</td>
      <td>G</td>
      <td>%47</td>
      <td>w</td>
      <td>%77</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%18</td>
      <td>H</td>
      <td>%48</td>
      <td>x</td>
      <td>%78</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%19</td>
      <td>I</td>
      <td>%49</td>
      <td>y</td>
      <td>%79</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%1a</td>
      <td>J</td>
      <td>%4a</td>
      <td>z</td>
      <td>%7a</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%1b</td>
      <td>K</td>
      <td>%4b</td>
      <td>{</td>
      <td>%7b</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%1c</td>
      <td>L</td>
      <td>%4c</td>
      <td>|</td>
      <td>%7c</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%1d</td>
      <td>M</td>
      <td>%4d</td>
      <td>}</td>
      <td>%7d</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%1e</td>
      <td>N</td>
      <td>%4e</td>
      <td>~</td>
      <td>%7e</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%1f</td>
      <td>O</td>
      <td>%4f</td>
      <td>&nbsp;</td>
      <td>%7f</td>
    </tr>
    <tr>
      <td>space</td>
      <td>%20</td>
      <td>P</td>
      <td>%50</td>
      <td>€</td>
      <td>%80</td>
    </tr>
    <tr>
      <td>!</td>
      <td>%21</td>
      <td>Q</td>
      <td>%51</td>
      <td>&nbsp;</td>
      <td>%81</td>
    </tr>
    <tr>
      <td>"</td>
      <td>%22</td>
      <td>R</td>
      <td>%52</td>
      <td>‚</td>
      <td>%82</td>
    </tr>
    <tr>
      <td>#</td>
      <td>%23</td>
      <td>S</td>
      <td>%53</td>
      <td>ƒ</td>
      <td>%83</td>
    </tr>
    <tr>
      <td>$</td>
      <td>%24</td>
      <td>T</td>
      <td>%54</td>
      <td>„</td>
      <td>%84</td>
    </tr>
    <tr>
      <td>%</td>
      <td>%25</td>
      <td>U</td>
      <td>%55</td>
      <td>…</td>
      <td>%85</td>
    </tr>
    <tr>
      <td>&amp;</td>
      <td>%26</td>
      <td>V</td>
      <td>%56</td>
      <td>†</td>
      <td>%86</td>
    </tr>
    <tr>
      <td>'</td>
      <td>%27</td>
      <td>W</td>
      <td>%57</td>
      <td>‡</td>
      <td>%87</td>
    </tr>
    <tr>
      <td>(</td>
      <td>%28</td>
      <td>X</td>
      <td>%58</td>
      <td>ˆ</td>
      <td>%88</td>
    </tr>
    <tr>
      <td>)</td>
      <td>%29</td>
      <td>Y</td>
      <td>%59</td>
      <td>‰</td>
      <td>%89</td>
    </tr>
    <tr>
      <td>*</td>
      <td>%2a</td>
      <td>Z</td>
      <td>%5a</td>
      <td>Š</td>
      <td>%8a</td>
    </tr>
    <tr>
      <td>+</td>
      <td>%2b</td>
      <td>[</td>
      <td>%5b</td>
      <td>‹</td>
      <td>%8b</td>
    </tr>
    <tr>
      <td>,</td>
      <td>%2c</td>
      <td>\</td>
      <td>%5c</td>
      <td>Œ</td>
      <td>%8c</td>
    </tr>
    <tr>
      <td>-</td>
      <td>%2d</td>
      <td>]</td>
      <td>%5d</td>
      <td>&nbsp;</td>
      <td>%8d</td>
    </tr>
    <tr>
      <td>.</td>
      <td>%2e</td>
      <td>^</td>
      <td>%5e</td>
      <td>Ž</td>
      <td>%8e</td>
    </tr>
    <tr>
      <td>/</td>
      <td>%2f</td>
      <td>_</td>
      <td>%5f</td>
      <td>&nbsp;</td>
      <td>%8f</td>
    </tr>
</tbody></table>

<table class="dataintable">
    <tbody><tr>
      <th>ASCII Value</th>
      <th>URL-encode</th>
      <th>ASCII Value</th>
      <th>URL-encode</th>
      <th>ASCII Value</th>
      <th>URL-encode</th>
      </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%90</td>
      <td>À</td>
      <td>%c0</td>
      <td>ð</td>
      <td>%f0</td>
    </tr>
    <tr>
      <td>‘</td>
      <td>%91</td>
      <td>Á</td>
      <td>%c1</td>
      <td>ñ</td>
      <td>%f1</td>
    </tr>
    <tr>
      <td>’</td>
      <td>%92</td>
      <td>Â</td>
      <td>%c2</td>
      <td>ò</td>
      <td>%f2</td>
    </tr>
    <tr>
      <td>“</td>
      <td>%93</td>
      <td>Ã</td>
      <td>%c3</td>
      <td>ó</td>
      <td>%f3</td>
    </tr>
    <tr>
      <td>”</td>
      <td>%94</td>
      <td>Ä</td>
      <td>%c4</td>
      <td>ô</td>
      <td>%f4</td>
    </tr>
    <tr>
      <td>•</td>
      <td>%95</td>
      <td>Å</td>
      <td>%c5</td>
      <td>õ</td>
      <td>%f5</td>
    </tr>
    <tr>
      <td>–</td>
      <td>%96</td>
      <td>Æ</td>
      <td>%c6</td>
      <td>ö</td>
      <td>%f6</td>
    </tr>
    <tr>
      <td>—</td>
      <td>%97</td>
      <td>Ç</td>
      <td>%c7</td>
      <td>÷</td>
      <td>%f7</td>
    </tr>
    <tr>
      <td>˜</td>
      <td>%98</td>
      <td>È</td>
      <td>%c8</td>
      <td>ø</td>
      <td>%f8</td>
    </tr>
    <tr>
      <td>™</td>
      <td>%99</td>
      <td>É</td>
      <td>%c9</td>
      <td>ù</td>
      <td>%f9</td>
    </tr>
    <tr>
      <td>š</td>
      <td>%9a</td>
      <td>Ê</td>
      <td>%ca</td>
      <td>ú</td>
      <td>%fa</td>
    </tr>
    <tr>
      <td>›</td>
      <td>%9b</td>
      <td>Ë</td>
      <td>%cb</td>
      <td>û</td>
      <td>%fb</td>
    </tr>
    <tr>
      <td>œ</td>
      <td>%9c</td>
      <td>Ì</td>
      <td>%cc</td>
      <td>ü</td>
      <td>%fc</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%9d</td>
      <td>Í</td>
      <td>%cd</td>
      <td>ý</td>
      <td>%fd</td>
    </tr>
    <tr>
      <td>ž</td>
      <td>%9e</td>
      <td>Î</td>
      <td>%ce</td>
      <td>þ</td>
      <td>%fe</td>
    </tr>
    <tr>
      <td>Ÿ</td>
      <td>%9f</td>
      <td>Ï</td>
      <td>%cf</td>
      <td>ÿ</td>
      <td>%ff</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%a0</td>
      <td>Ð</td>
      <td>%d0</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>¡</td>
      <td>%a1</td>
      <td>Ñ</td>
      <td>%d1</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>¢</td>
      <td>%a2</td>
      <td>Ò</td>
      <td>%d2</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>£</td>
      <td>%a3</td>
      <td>Ó</td>
      <td>%d3</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>%a4</td>
      <td>Ô</td>
      <td>%d4</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>¥</td>
      <td>%a5</td>
      <td>Õ</td>
      <td>%d5</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>|</td>
      <td>%a6</td>
      <td>Ö</td>
      <td>%d6</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>§</td>
      <td>%a7</td>
      <td>&nbsp;</td>
      <td>%d7</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>¨</td>
      <td>%a8</td>
      <td>Ø</td>
      <td>%d8</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>©</td>
      <td>%a9</td>
      <td>Ù</td>
      <td>%d9</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>ª</td>
      <td>%aa</td>
      <td>Ú</td>
      <td>%da</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>«</td>
      <td>%ab</td>
      <td>Û</td>
      <td>%db</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>¬</td>
      <td>%ac</td>
      <td>Ü</td>
      <td>%dc</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>¯</td>
      <td>%ad</td>
      <td>Ý</td>
      <td>%dd</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>®</td>
      <td>%ae</td>
      <td>Þ</td>
      <td>%de</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>¯</td>
      <td>%af</td>
      <td>ß</td>
      <td>%df</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>°</td>
      <td>%b0</td>
      <td>à</td>
      <td>%e0</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>±</td>
      <td>%b1</td>
      <td>á</td>
      <td>%e1</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>²</td>
      <td>%b2</td>
      <td>â</td>
      <td>%e2</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>³</td>
      <td>%b3</td>
      <td>ã</td>
      <td>%e3</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>´</td>
      <td>%b4</td>
      <td>ä</td>
      <td>%e4</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>µ</td>
      <td>%b5</td>
      <td>å</td>
      <td>%e5</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>¶</td>
      <td>%b6</td>
      <td>æ</td>
      <td>%e6</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>·</td>
      <td>%b7</td>
      <td>ç</td>
      <td>%e7</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>¸</td>
      <td>%b8</td>
      <td>è</td>
      <td>%e8</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>¹</td>
      <td>%b9</td>
      <td>é</td>
      <td>%e9</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>º</td>
      <td>%ba</td>
      <td>ê</td>
      <td>%ea</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>»</td>
      <td>%bb</td>
      <td>ë</td>
      <td>%eb</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>¼</td>
      <td>%bc</td>
      <td>ì</td>
      <td>%ec</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>½</td>
      <td>%bd</td>
      <td>í</td>
      <td>%ed</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>¾</td>
      <td>%be</td>
      <td>î</td>
      <td>%ee</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>¿</td>
      <td>%bf</td>
      <td>ï</td>
      <td>%ef</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
</tbody></table>
</details>

<div id="httpmessage"></div><br />
<a href="#reference">top</a><br />
<a href="#top">pagetop</a>

<details open>
<summary>Http Message</summary>


<div>
<h2>1xx: 信息</h2>

<table class="dataintable">
  <tbody><tr>
    <th style="width:40%;">消息:</th>
    <th style="width:60%;">描述:</th>
  </tr>
  <tr>
    <td>100 Continue</td>
    <td>服务器仅接收到部分请求，但是一旦服务器并没有拒绝该请求，客户端应该继续发送其余的请求。</td>
  </tr>
  <tr>
    <td>101 Switching Protocols</td>
    <td>服务器转换协议：服务器将遵从客户的请求转换到另外一种协议。</td>
  </tr>
</tbody></table>
</div>

<div>
<h2>2xx: 成功</h2>

<table class="dataintable">
  <tbody><tr>
    <th style="width:40%;">消息:</th>
    <th style="width:60%;">描述:</th>
  </tr>
  <tr>
    <td>200 OK</td>
    <td>请求成功（其后是对GET和POST请求的应答文档。）</td>
  </tr>
  <tr>
    <td>201 Created</td>
    <td>请求被创建完成，同时新的资源被创建。</td>
  </tr>
  <tr>
    <td>202 Accepted</td>
    <td>供处理的请求已被接受，但是处理未完成。</td>
  </tr>
  <tr>
    <td>203 Non-authoritative Information</td>
    <td>文档已经正常地返回，但一些应答头可能不正确，因为使用的是文档的拷贝。</td>
  </tr>
  <tr>
    <td>204 No Content</td>
    <td>没有新文档。浏览器应该继续显示原来的文档。如果用户定期地刷新页面，而Servlet可以确定用户文档足够新，这个状态代码是很有用的。</td>
  </tr>
  <tr>
    <td>205 Reset Content</td>
    <td>没有新文档。但浏览器应该重置它所显示的内容。用来强制浏览器清除表单输入内容。</td>
  </tr>
  <tr>
    <td>206 Partial Content</td>
    <td>客户发送了一个带有Range头的GET请求，服务器完成了它。</td>
  </tr>
</tbody></table>
</div>

<div>
<h2>3xx: 重定向</h2>

<table class="dataintable">
  <tbody><tr>
    <th style="width:40%;">消息:</th>
    <th style="width:60%;">描述:</th>
  </tr>
  <tr>
    <td>300 Multiple Choices</td>
    <td>多重选择。链接列表。用户可以选择某链接到达目的地。最多允许五个地址。</td>
  </tr>
  <tr>
    <td>301 Moved Permanently</td>
    <td>所请求的页面已经转移至新的url。</td>
  </tr>
  <tr>
    <td>302 Found</td>
    <td>所请求的页面已经临时转移至新的url。</td>
  </tr>
  <tr>
    <td>303 See Other</td>
    <td>所请求的页面可在别的url下被找到。</td>
  </tr>
  <tr>
    <td>304 Not Modified</td>
    <td>未按预期修改文档。客户端有缓冲的文档并发出了一个条件性的请求（一般是提供If-Modified-Since头表示客户只想比指定日期更新的文档）。服务器告诉客户，原来缓冲的文档还可以继续使用。</td>
  </tr>
  <tr>
    <td>305 Use Proxy</td>
    <td>客户请求的文档应该通过Location头所指明的代理服务器提取。</td>
  </tr>
  <tr>
    <td>306 <i>Unused</i></td>
    <td>此代码被用于前一版本。目前已不再使用，但是代码依然被保留。</td>
  </tr>
  <tr>
    <td>307 Temporary Redirect</td>
    <td>被请求的页面已经临时移至新的url。</td>
  </tr>
</tbody></table>
</div>

<div>
<h2>4xx: 客户端错误</h2>

<table class="dataintable">
  <tbody><tr>
    <th style="width:40%;">消息:</th>
    <th style="width:60%;">描述:</th>
  </tr>
  <tr>
    <td>400 Bad Request</td>
    <td>服务器未能理解请求。</td>
  </tr>
  <tr>
    <td>401 Unauthorized</td>
    <td>被请求的页面需要用户名和密码。</td>
  </tr>
  <tr>
    <td>402 Payment Required</td>
    <td>此代码尚无法使用。</td>
  </tr>
  <tr>
    <td>403 Forbidden</td>
    <td>对被请求页面的访问被禁止。</td>
  </tr>
  <tr>
    <td>404 Not Found</td>
    <td>服务器无法找到被请求的页面。</td>
  </tr>
  <tr>
    <td>405 Method Not Allowed</td>
    <td>请求中指定的方法不被允许。</td>
  </tr>
  <tr>
    <td>406 Not Acceptable</td>
    <td>服务器生成的响应无法被客户端所接受。</td>
  </tr>
  <tr>
    <td>407 Proxy Authentication Required</td>
    <td>用户必须首先使用代理服务器进行验证，这样请求才会被处理。</td>
  </tr>
  <tr>
    <td>408 Request Timeout</td>
    <td>请求超出了服务器的等待时间。</td>
  </tr>
  <tr>
    <td>409 Conflict</td>
    <td>由于冲突，请求无法被完成。</td>
  </tr>
  <tr>
    <td>410 Gone</td>
    <td>被请求的页面不可用。</td>
  </tr>
  <tr>
    <td>411 Length Required</td>
    <td>"Content-Length" 未被定义。如果无此内容，服务器不会接受请求。</td>
  </tr>
  <tr>
    <td>412 Precondition Failed</td>
    <td>请求中的前提条件被服务器评估为失败。</td>
  </tr>
  <tr>
    <td>413 Request Entity Too Large</td>
    <td>由于所请求的实体的太大，服务器不会接受请求。</td>
  </tr>
  <tr>
    <td>414 Request-url Too Long</td>
    <td>由于url太长，服务器不会接受请求。当post请求被转换为带有很长的查询信息的get请求时，就会发生这种情况。</td>
  </tr>
  <tr>
    <td>415 Unsupported Media Type</td>
    <td>由于媒介类型不被支持，服务器不会接受请求。</td>
  </tr>
  <tr>
    <td>416&nbsp;</td>
    <td>服务器不能满足客户在请求中指定的Range头。</td>
  </tr>
  <tr>
    <td>417 Expectation Failed</td>
    <td>&nbsp;</td>
  </tr>
</tbody></table>
</div>

<div>
<h2>5xx: 服务器错误</h2>

<table class="dataintable">
  <tbody><tr>
    <th style="width:40%;">消息:</th>
    <th style="width:60%;">描述:</th>
  </tr>
  <tr>
    <td>500&nbsp;Internal Server Error</td>
    <td>请求未完成。服务器遇到不可预知的情况。</td>
  </tr>
  <tr>
    <td>501 Not Implemented</td>
    <td>请求未完成。服务器不支持所请求的功能。</td>
  </tr>
  <tr>
    <td>502 Bad Gateway</td>
    <td>请求未完成。服务器从上游服务器收到一个无效的响应。</td>
  </tr>
  <tr>
    <td>503 Service Unavailable</td>
    <td>请求未完成。服务器临时过载或当机。</td>
  </tr>
  <tr>
    <td>504 Gateway Timeout</td>
    <td>网关超时。</td>
  </tr>
  <tr>
    <td>505 HTTP Version Not Supported</td>
    <td>服务器不支持请求中指明的HTTP协议版本。</td>
  </tr>
</tbody></table>
</div>

</details>

<div id="httpmethod"></div><br />
<a href="#reference">top</a><br />
<a href="#top">pagetop</a>

<details open>
<summary>Http Method</summary>
<table class="dataintable">
<tbody><tr>
<th style="width:20%;">&nbsp;</th>
<th>GET</th>
<th>POST</th>
</tr>

<tr>
<td>后退按钮/刷新</td>
<td>无害</td>
<td>数据会被重新提交（浏览器应该告知用户数据会被重新提交）。</td>
</tr>

<tr>
<td>书签</td>
<td>可收藏为书签</td>
<td>不可收藏为书签</td>
</tr>

<tr>
<td>缓存</td>
<td>能被缓存</td>
<td>不能缓存</td>
</tr>

<tr>
<td>编码类型</td>
<td>application/x-www-form-urlencoded</td>
<td>application/x-www-form-urlencoded 或 multipart/form-data。为二进制数据使用多重编码。</td>
</tr>

<tr>
<td>历史</td>
<td>参数保留在浏览器历史中。</td>
<td>参数不会保存在浏览器历史中。</td>
</tr>

<tr>
<td>对数据长度的限制</td>
<td>是的。当发送数据时，GET 方法向 URL 添加数据；URL 的长度是受限制的（URL 的最大长度是 2048 个字符）。</td>
<td>无限制。</td>
</tr>

<tr>
<td>对数据类型的限制</td>
<td>只允许 ASCII 字符。</td>
<td>没有限制。也允许二进制数据。</td>
</tr>

<tr>
<td>安全性</td>
<td><p>与 POST 相比，GET 的安全性较差，因为所发送的数据是 URL 的一部分。</p>
<p>在发送密码或其他敏感信息时绝不要使用 GET ！</p></td>
<td>POST 比 GET 更安全，因为参数不会被保存在浏览器历史或 web 服务器日志中。</td>
</tr>

<tr>
<td>可见性</td>
<td>数据在 URL 中对所有人都是可见的。</td>
<td>数据不会显示在 URL 中。</td>
</tr>
</tbody></table>
</details>


<div id="globalproperty"></div><br />
<a href="#reference">top</a><br />
<a href="#top">pagetop</a>

<details open>

<summary>GlobalProperty</summary>

# accesskey
```html
<a href="http://baidu.com" accesskey="h">HTML</a><br />
<a href="http://bing.com" accesskey="c">CSS</a>
```
<a href="http://baidu.com" accesskey="h">HTML 教程</a><br />
<a href="http://bing.com" accesskey="c">CSS 教程</a>

<p><b>注释：</b>请使用Alt + <i>accessKey</i> (或者 Shift + Alt + <i>accessKey</i>) 来访问带有指定快捷键的元素。</p>

## class

## contenteditable <sup>5np</sup>
```html
<p contenteditable="true"> hello world</p>
```
<p contenteditable="true">hello world</p>

## contextmenu <sup>5np</sup>

## data-* <sup>5np</sup>
```html
<script>
function showDetails(animal) {
    var animalType = animal.getAttribute("data-animal-type");
    alert(animal.innerHTML + "是一种" + animalType + "。");
}
</script>
<ul>
  <li onclick="showDetails(this)" id="owl" data-animal-type="鸟类">喜鹊</li>
  <li onclick="showDetails(this)" id="salmon" data-animal-type="鱼类">金枪鱼</li>
  <li onclick="showDetails(this)" id="tarantula" data-animal-type="蜘蛛">蝇虎</li>
</ul>
```
<script>
function showDetails(animal) {
    var animalType = animal.getAttribute("data-animal-type");
    alert(animal.innerHTML + "是一种" + animalType + "。");
}
</script>
<h1>物种</h1>

<p>点击某个物种来查看其类别：</p>

<ul>
  <li onclick="showDetails(this)" id="owl" data-animal-type="鸟类">喜鹊</li>
  <li onclick="showDetails(this)" id="salmon" data-animal-type="鱼类">金枪鱼</li>
  <li onclick="showDetails(this)" id="tarantula" data-animal-type="蜘蛛">蝇虎</li>
</ul>

[https://www.w3school.com.cn/tags/att_global_data.asp](https://www.w3school.com.cn/tags/att_global_data.asp)

## dir
```html
<p dir="rtl">Write this text right-to-left!</p>
```
<p dir="rtl">Write this text right-to-left!</p>

[https://www.w3school.com.cn/tags/att_standard_dir.asp](https://www.w3school.com.cn/tags/att_standard_dir.asp)


## deaggable <sup>5np</sup>
```html
<script type="text/javascript">
function allowDrop(ev)
{
ev.preventDefault();
}

function drag(ev)
{
ev.dataTransfer.setData("Text",ev.target.id);
}

function drop(ev)
{
var data=ev.dataTransfer.getData("Text");
ev.target.appendChild(document.getElementById(data));
ev.preventDefault();
}
</script>
<div id="div1" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
<br />
<p id="drag1" draggable="true" ondragstart="drag(event)">这是一段可移动的段落。请把该段落拖入上面的矩形。</p>
```
<script type="text/javascript">
function allowDrop(ev)
{
ev.preventDefault();
}

function drag(ev)
{
ev.dataTransfer.setData("Text",ev.target.id);
}

function drop(ev)
{
var data=ev.dataTransfer.getData("Text");
ev.target.appendChild(document.getElementById(data));
ev.preventDefault();
}
</script>
<div id="div1" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
<br />
<p id="drag1" draggable="true" ondragstart="drag(event)">这是一段可移动的段落。请把该段落拖入上面的矩形。</p>

## dropzone <sup>5np</sup>

## hidden <sup>5np</sup>
```html
<p hidden> hidden </p>
```
<p hidden> hidden</p>

## id

## lang

## spellcheck <sup>5np</sup>

## style

## tabindex
```html
<a href="http://www.w3school.com.cn/" tabindex="2">W3School</a>
<a href="http://www.google.com/" tabindex="1">Google</a>
<a href="http://www.microsoft.com/" tabindex="3">Microsoft</a>
```
<a href="http://www.w3school.com.cn/" tabindex="2">W3School</a>
<a href="http://www.google.com/" tabindex="1">Google</a>
<a href="http://www.microsoft.com/" tabindex="3">Microsoft</a>

## title
```html
<abbr title="People's Republic of China">PRC</abbr> was founded in 1949.
<p title="Free Web tutorials">W3School.com.cn</p>
```
<abbr title="People's Republic of China">PRC</abbr> was founded in 1949.
<p title="Free Web tutorials">W3School.com.cn</p>

## translate
</details>
</details>


<div id="htmlelement"></div><br />
<a href="#top">pagetop</a>


<details open>
<summary style="color: red;font-size: 60px">Html Element</summary>
<div id="html5elementtop"></div>

<a href="#!doctype">!doctype</a> <br />
<a href="#a">a</a><br />
<a href="#abbr">abbr</a><br />
<a href="#acronym">acronym</a><br />
<a href="#address">address</a><br />
<a href="#applet">applet</a><br />
<a href="#area">area</a><br />
<a class="dbig" href="#article">article</a><br />
<a class="dbig" href="#aside">aside</a><br />
<a class="dbig" href="#audio">audio</a><br />
<a href="#b">b</a><br />
<a href="#base">base</a><br />
<a href="#basefont">basefont</a><br />
<a class="dbig" href="#bdi">bdi</a><br />
<a href="#big">big</a><br />
<a href="#blockquote">blockquote</a><br />
<a href="#body">body</a><br />
<a href="#br">br</a><br />
<a href="#button">button</a><br />
<a href="#canvas">canvas</a><br />
<a href="#caption">caption</a><br />
<a href="#center">center</a><br />
<a href="#cite">cite</a><br />
<a href="#code">code</a><br />
<a href="#col">col</a><br />
<a href="#colgroup">colgroup</a><br />
<a href="#command">command</a><br />
<a class="dbig" href="#details">details</a><br />
<a href="#dd">dd</a><br />
<a href="#del">del</a><br />
<a href="#dir">dir</a><br />
<a href="#div">div</a><br />
<a class="dbig" href="#dialog">dialog</a><br />
<a href="#em">em</a><br />
<a href="#embed">embed</a><br />
<a href="#fieldset">fieldset</a><br />
<a class="dbig" href="#figcaption">figcaption</a><br />
<a class="dbig" href="#figure">figure</a><br />
<a href="#font">font</a><br />
<a class="dbig" href="#footer">footer</a><br />
<a href="#form">form</a><br />
<a href="#frame">frame</a><br />
<a href="#frameset">frameset</a><br />
<a href="#h1">h1</a><br />
<a href="#head">head</a><br />
<a class="dbig" href="#header">header</a><br />
<a href="#hr">hr</a><br />
<a href="#i">i</a><br />
<a href="#iframe">iframe</a><br />
<a href="#img">img</a><br />
<a href="#input">input</a><br />
<a href="#ins">ins</a><br />
<a href="#kbd">kbd</a><br />
<a href="#keygen">keygen</a><br />
<a href="#label">label</a><br />
<a href="#legend">legend</a><br />
<a href="#li">li</a><br />
<a href="#link">link</a><br />
<a class="dbig" href="#main">main</a><br />
<a href="#map">map</a><br />
<a class="dbig" href="#mark">mark</a><br />
<a href="#menu">menu</a><br />
<a class="dbig" href="#menuitem">menuitem</a><br />
<a href="#meta">meta</a><br />
<a class="dbig" href="#meter">meter</a><br />
<a class="dbig" href="#nav">nav</a><br />
<a href="#noframes">noframes</a><br />
<a href="#noscript">noscript</a><br />
<a href="#object">object</a><br />
<a href="#ol">ol</a><br />
<a href="#optgroup">optgroup</a><br />
<a href="#option">option</a><br />
<a href="#output">output</a><br />
<a href="#p">p</a><br />
<a href="#param">param</a><br />
<a href="#pre">pre</a><br />
<a class="dbig" href="#progress">progress</a><br />
<a href="#q">q</a><br />
<a class="dbig" href="#rp">rp</a><br />
<a href="#s">s</a><br />
<a href="#samp">samp</a><br />
<a href="#script">script</a><br />
<a class="dbig" href="#section">section</a><br />
<a href="#select">select</a><br />
<a href="#small">small</a><br />
<a href="#source">source</a><br />
<a href="#span">span</a><br />
<a href="#strike">strike</a><br />
<a href="#strong">strong</a><br />
<a href="#style">style</a><br />
<a href="#sub">sub</a><br />
<a class="dbig" href="#summary">summary</a><br />
<a href="#sup">sup</a><br />
<a href="#table">table</a><br />
<a href="#tbody">tbody</a><br />
<a href="#td">td</a><br />
<a href="#textarea">textarea</a><br />
<a href="#tfoot">tfoot</a><br />
<a href="#th">th</a><br />
<a href="#thead">thead</a><br />
<a class="dbig" href="#time">time</a><br />
<a href="#title">title</a><br />
<a href="#tr">tr</a><br />
<a href="#track">track</a><br />
<a href="#ul">ul</a><br />
<a class="dbig" href="#video">video</a><br />
<a class="dbig" href="#wbr">wbr</a><br />

# define
  **<sup>5np</sup>**: means label has *<sup>new property in html5<sup>* version, you can checkout link below every example  
  **<sup>5n</sup>**: means *<sup>new label</sup>* in html5 version , you can checkout link below every example  
  **<sup>X</sup>**: not support in html5 OR not suggest use  
# ```<!--...-->```
> define comment

<div id="!doctype"></div>

# <!DOCTYPE>
> define document type
  ```html
<!DOCTYPE html> #define this document is belong html5
```

<div id="a"></div>

# \<a> <sup>5np</sup>
> define anchor
```html
<a href="http://www.w3school.com.cn">W3School</a>
```
[https://www.w3school.com.cn/tags/tag_a.asp](https://www.w3school.com.cn/tags/tag_a.asp)

<div id="abbr"></div>
<a href="#html5elementtop">top</a>

# \<abbr>
> define abbreviation
```html
The <abbr title="People's Republic of China">PRC</abbr> was founded in 1949.
```
The <abbr title="People's Republic of China">PRC</abbr> was founded in 1949.

[https://www.w3school.com.cn/tags/tag_abbr.asp](https://www.w3school.com.cn/tags/tag_abbr.asp)

<div id="acronym"></div>
<a href="#html5elementtop">top</a>

# <sup>X</sup>\<acronym>
> like abbr label; not support in html5
```html
<acronym title="World Wide Web">WWW</acronym>
```
<acronym title="World Wide Web">WWW</acronym>

[https://www.w3school.com.cn/tags/tag_acronym.asp](https://www.w3school.com.cn/tags/tag_acronym.asp)

<div id="address"></div>
<a href="#html5elementtop">top</a>

# \<address>
> define author and contact
```html
<address>
Written by <a href="mailto:webmaster@example.com">Donald Duck</a>.<br>
Visit us at:<br>
Example.com<br>
Box 564, Disneyland<br>
USA
</address>
```
<address>
Written by <a href="mailto:webmaster@example.com">Donald Duck</a>.<br>
Visit us at:<br>
Example.com<br>
Box 564, Disneyland<br>
USA
</address>

[https://www.w3school.com.cn/tags/tag_address.asp](https://www.w3school.com.cn/tags/tag_address.asp)

<div id="applet"></div>
<a href="#html5elementtop">top</a>

# X\<applet>
> define embed applet, not suggest use, not support in html5, please replace by object
```html
<applet code="Bubbles.class" width="350" height="350">
Java applet that draws animated bubbles.
</applet>
```
<applet code="Bubbles.class" width="350" height="350">
Java applet that draws animated bubbles.
</applet>

[https://www.w3school.com.cn/tags/tag_applet.asp](https://www.w3school.com.cn/tags/tag_applet.asp)

<div id="area"></div>
<a href="#html5elementtop">top</a>

# \<area>
> define picture maping area
```html
<img src="https://www.w3school.com.cn/i/eg_planets.jpg" border="0" usemap="#planetmap" alt="Planets" />

<map name="planetmap" id="planetmap">
  <area shape="circle" coords="180,139,14" href ="venus.html" alt="Venus" />
  <area shape="circle" coords="129,161,10" href ="mercur.html" alt="Mercury" />
  <area shape="rect" coords="0,0,110,260" href ="sun.html" alt="Sun" />
</map>
```
<img src="https://www.w3school.com.cn/i/eg_planets.jpg" border="0" usemap="#planetmap" alt="Planets" />

<map name="planetmap" id="planetmap">
  <area shape="circle" coords="180,139,14" href ="venus.html" alt="Venus" />
  <area shape="circle" coords="129,161,10" href ="mercur.html" alt="Mercury" />
  <area shape="rect" coords="0,0,110,260" href ="sun.html" alt="Sun" />
</map>

[https://www.w3school.com.cn/tags/tag_area.asp](https://www.w3school.com.cn/tags/tag_area.asp)

<div id="article"></div>
<a href="#html5elementtop">top</a>

# \<article> <sup>5n</sup>
> define article
> post blog newspaper comment
```html
<article>
  <h1>Internet Explorer 9</h1>
  <p>Windows Internet Explorer 9（简称 IE9）于 2011 年 3 月 14 日发布.....</p>
</article>
```
<article>
  <h1>Internet Explorer 9</h1>
  <p>Windows Internet Explorer 9（简称 IE9）于 2011 年 3 月 14 日发布.....</p>
</article>

[https://www.w3school.com.cn/tags/tag_article.asp](https://www.w3school.com.cn/tags/tag_article.asp)

<div id="aside"></div>
<a href="#html5elementtop">top</a>

# \<aside> <sup>5n</sup>
> define outside of the page content, artile's sidebar
```html
<p>Me and my family visited The Epcot center this summer.</p>
<aside>
<h4>Epcot Center</h4>
The Epcot Center is a theme park in Disney World, Florida.
</aside>
```

<p>Me and my family visited The Epcot center this summer.</p>
<aside>
<h4>Epcot Center</h4>
The Epcot Center is a theme park in Disney World, Florida.
</aside>

[https://www.w3school.com.cn/tags/tag_aside.asp](https://www.w3school.com.cn/tags/tag_aside.asp)

<div id="audio"></div>
<a href="#html5elementtop">top</a>

# \<audio> <sup>5n</sup>
> defint sound content
```html
<audio src="/i/horse.ogg" controls="controls">
Your browser does not support the audio element.
</audio>
```
<audio src="https://www.w3school.com.cn/i/horse.ogg" controls="controls">
Your browser does not support the audio element.
</audio>

[https://www.w3school.com.cn/tags/tag_audio.asp](https://www.w3school.com.cn/tags/tag_audio.asp)

<div id="b"></div>
<a href="#html5elementtop">top</a>

# \<b>
> defint bold font
```html
<b>hello world!!</b>
```
<b>hello world!!</b>

<div id="base"></div>
<a href="#html5elementtop">top</a>

# \<base>
> define all of the link default arm in page
```html
<head>
<base href="http://www.w3school.com.cn/i/" />
<base target="_blank" />
</head>

<body>
<img src="eg_smile.gif" />
<a href="http://www.w3school.com.cn">W3School</a>
</body>
```
[https://www.w3school.com.cn/tags/tag_base.asp](https://www.w3school.com.cn/tags/tag_base.asp)

<div id="basefont"></div>
<a href="#html5elementtop">top</a>

# X\<basefont>
> define default font family color and size, not suggest use,please replace by css style
```html
<head>
<basefont color="red" size="5" />
</head>

<body>
<h1>This is a header</h1>
<p>This is a paragraph</p>
</body>
```

<div id="bdi"></div>
<a href="#html5elementtop">top</a>

# \<bdi> <sup>5n</sup>
> defint text direction
```html
<ul>
   <li><bdo dir="rtl">Username <bdi dir="ltr">Bill</bdi>:80 points</bdo></li>
   <li>Username <bdi>Steve</bdi>: 78 points</li>
</ul>
```

<ul>
<li><bdo dir="rtl">Username <bdi dir="ltr">Bill</bdi>:80 points</bdo></li>
<li>Username <bdi>Steve</bdi>: 78 points</li>
</ul>

[https://www.w3school.com.cn/tags/tag_bdi.asp](https://www.w3school.com.cn/tags/tag_bdi.asp)

<div id="big"></div>
<a href="#html5elementtop">top</a>

# <sup>X</sup>\<big>
> deine big text
```html
<big>hello</big>
```
<big>hello</big>

[https://www.w3school.com.cn/tags/tag_font_style.asp](https://www.w3school.com.cn/tags/tag_font_style.asp)

<div id="blockquote"></div>
<a href="#html5elementtop">top</a>

# \<blockquote>
>define long quote
```html
<blockquote>
Here is a long quotation here is a long quotation here is a long quotation
here is a long quotation here is a long quotation here is a long quotation
here is a long quotation here is a long quotation here is a long quotation.
</blockquote>
```
<blockquote>
Here is a long quotation here is a long quotation here is a long quotation
here is a long quotation here is a long quotation here is a long quotation
here is a long quotation here is a long quotation here is a long quotation.
</blockquote>

[https://www.w3school.com.cn/tags/tag_blockquote.asp](https://www.w3school.com.cn/tags/tag_blockquote.asp)

<div id="body"></div>
<a href="#html5elementtop">top</a>

# \<body>
>define main document

<div id="br"></div>
<a href="#html5elementtop">top</a>

# \<br>
>define change line char
```html
<br> # in xhtml <br />
```
11
<br>
11

<div id="button"></div>
<a href="#html5elementtop">top</a>

# \<button><sup>5p</sup>
>define a button
```html
<button type="button">Click Me!</button>
```
<button type="button">Click Me!</button>

[https://www.w3school.com.cn/tags/tag_button.asp](https://www.w3school.com.cn/tags/tag_button.asp)

<div id="canvas"></div>
<a href="#html5elementtop">top</a>

# \<canvas><sup>5n</sup>
> define graph
```html
<canvas id="myCanvas"></canvas>

<script type="text/javascript">

var canvas=document.getElementById('myCanvas');
var ctx=canvas.getContext('2d');
ctx.fillStyle='#FF0000';
ctx.fillRect(0,0,80,100);

</script>
```
<canvas id="myCanvas"></canvas>

<script type="text/javascript">

var canvas=document.getElementById('myCanvas');
var ctx=canvas.getContext('2d');
ctx.fillStyle='#FF0000';
ctx.fillRect(0,0,80,100);

</script>

[https://www.w3school.com.cn/tags/tag_canvas.asp](https://www.w3school.com.cn/tags/tag_canvas.asp)

<div id="caption"></div>
<a href="#html5elementtop">top</a>

# \<caption>
> define table title
```html
<table border="1">
  <caption>Monthly savings</caption>
  <tr>
    <th>Month</th>
    <th>Savings</th>
  </tr>
  <tr>
    <td>January</td>
    <td>$100</td>
  </tr>
</table>
```

<table border="1">
  <caption>Monthly savings</caption>
  <tr>
    <th>Month</th>
    <th>Savings</th>
  </tr>
  <tr>
    <td>January</td>
    <td>$100</td>
  </tr>
</table>

[https://www.w3school.com.cn/tags/tag_caption.asp](https://www.w3school.com.cn/tags/tag_caption.asp)

<div id="center"></div>
<a href="#html5elementtop">top</a>

# <sup>X</sup>\<center>
> alian text to center, not suggest use
```html
<center></center>
```

<div id="cite"></div>
<a href="#html5elementtop">top</a>

# \<cite>
> define quote
>\<cite> 标签通常表示它所包含的文本对某个参考文献的引用，比如书籍或者杂志的标题。按照惯例，引用的文本将以斜体显示。用 \<cite> 标签把指向其他文档的引用分离出来，尤其是分离那些传统媒体中的文档，如书籍、杂志、期刊，等等。如果引用的这些文档有联机版本，还应该把引用包括在一个 \<a> 标签中，从而把一个超链接指向该联机版本。
\<cite> 标签还有一个隐藏的功能：它可以使你或者其他人从文档中自动摘录参考书目。我们可以很容易地想象一个浏览器，它能够自动整理引用表格，并把它们作为脚注或者独立的文档来显示。\<cite> 标签的语义已经远远超过了改变它所包含的文本外观的作用；它使浏览器能够以各种实用的方式来向用户表达文档的内容。
```html
<a href="#"><cite>this is a title</cite></a>
```
<a href="#"><cite>this is a title</cite></a>

<div id="code"></div>
<a href="#html5elementtop">top</a>

# \<code>
> define code text
```html
<code>
# include <stdio.h>
int main(){printf("hello world!!");return 0;}
</code>
```
<code>
\# include <stdio.h>
int main(){printf("hello world!!");return 0;}
</code>

<div id="col"></div>
<a href="#html5elementtop">top</a>

# \<col>
> define muti col
```html
<table width="100%" border="1">
  <col align="left" />
  <col align="left" />
  <col align="right" />
  <tr>
    <th>ISBN</th>
    <th>Title</th>
    <th>Price</th>
  </tr>
  <tr>
    <td>3476896</td>
    <td>My first HTML</td>
    <td>$53</td>
  </tr>
</table>
```
<table width="100%" border="1">
  <col align="left" />
  <col align="left" />
  <col align="right" />
  <tr>
    <th>ISBN</th>
    <th>Title</th>
    <th>Price</th>
  </tr>
  <tr>
    <td>3476896</td>
    <td>My first HTML</td>
    <td>$53</td>
  </tr>
</table>

[https://www.w3school.com.cn/tags/tag_col.asp](https://www.w3school.com.cn/tags/tag_col.asp)

<div id="colgroup"></div>
<a href="#html5elementtop">top</a>

# \<colgroup>
> Column groups in the table for formatting
```html
<table width="100%" border="1">
  <colgroup span="2" align="left"></colgroup>
  <colgroup align="right" style="color:#0000FF;"></colgroup>
  <tr>
    <th>ISBN</th>
    <th>Title</th>
    <th>Price</th>
  </tr>
  <tr>
    <td>3476896</td>
    <td>My first HTML</td>
    <td>$53</td>
  </tr>
</table>
```
<table width="100%" border="1">
  <colgroup span="2" align="left"></colgroup>
  <colgroup align="right" style="color:#0000FF;"></colgroup>
  <tr>
    <th>ISBN</th>
    <th>Title</th>
    <th>Price</th>
  </tr>
  <tr>
    <td>3476896</td>
    <td>My first HTML</td>
    <td>$53</td>
  </tr>
</table>

<div id="command"></div>
<a href="#html5elementtop">top</a>

# <sup>X</sup>\<command><sup>5n</sup>
> define command buttons

```html
<menu>
<command onclick="alert('Hello World')">
Click Me!</command>
</menu>
```
<menu>
<command onclick="alert('Hello World')">
Click Me!</command>
</menu>

<div id="details"></div>
<a href="#html5elementtop">top</a>

# \<details> <sup>5n</sup>
> define dropdown list
```html
<details open>
<summary>hello</summary>
<p>ALl pages</p>
</details>
```
<details open="open">
<summary>hello</summary>
<p style>All pages</p>
</details>

[https://www.w3school.com.cn/tags/tag_details.asp](https://www.w3school.com.cn/tags/tag_details.asp)

<div id="dd"></div>
<a href="#html5elementtop">top</a>

# \<dd>
>Define the description of the items in the list
```html
<dl>
   <dt>计算机</dt>
   <dd>用来计算的仪器 ... ...</dd>
   <dt>显示器</dt>
   <dd>以视觉方式显示信息的装置 ... ...</dd>
</dl>
```
<dl>
   <dt>计算机</dt>
   <dd>用来计算的仪器 ... ...</dd>
   <dt>显示器</dt>
   <dd>以视觉方式显示信息的装置 ... ...</dd>
</dl>

[https://www.w3school.com.cn/tags/tag_dd.asp](https://www.w3school.com.cn/tags/tag_dd.asp)

<div id="del"></div>
<a href="#html5elementtop">top</a>

# \<del>
> define delete text
```html
a dozen is <del>20</del> 12 pieces
```
a dozen is <del>20</del> 12 pieces

[https://www.w3school.com.cn/tags/tag_del.asp](https://www.w3school.com.cn/tags/tag_del.asp)

<div id="dir"></div>
<a href="#html5elementtop">top</a>


# <sup>X</sup>\<dir>
> define Directory List
```html
<dir>
   <li>HTML</li>
   <li>XHTML</li>
   <li>CSS</li>
</dir>
```
<dir>
   <li>HTML</li>
   <li>XHTML</li>
   <li>CSS</li>
</dir>

<div id="div"></div>
<a href="#html5elementtop">top</a>

# \<div>
> define Sections in the document
```html
<div style="color:#00FF00">
  <h3>This is a header</h3>
  <p>This is a paragraph.</p>
</div>
```
<div style="color:#00FF00">
  <h3>This is a header</h3>
  <p>This is a paragraph.</p>
</div>

[https://www.w3school.com.cn/tags/tag_div.asp](https://www.w3school.com.cn/tags/tag_div.asp)

<div id="dialog"></div>
<a href="#html5elementtop">top</a>

# <sup>5n</sup>\<dialog>
> define Dialog or window
```html
<table border="1">
<tr>
  <th>一月 <dialog open>这是打开的对话窗口</dialog></th>
  <th>二月</th>
  <th>三月</th>
</tr>
<tr>
  <td>31</td>
  <td>28</td>
  <td>31</td>
</tr>
</table>
```
<table border="1">
<tr>
  <th>一月 <dialog open>这是打开的对话窗口</dialog></th>
  <th>二月</th>
  <th>三月</th>
</tr>
<tr>
  <td>31</td>
  <td>28</td>
  <td>31</td>
</tr>
</table>

[https://www.w3school.com.cn/tags/tag_dialog.asp](https://www.w3school.com.cn/tags/tag_dialog.asp)

<div id="em"></div>
<a href="#html5elementtop">top</a>

# \<em>
> define stress text
```html
<em>hello</em>
```
<em>hello</em>

<div id="embed"></div>
<a href="#html5elementtop">top</a>

# \<embed> <sup>5n</sup>
> define Embedded content
```html
<embed src="helloworld.swf" />
```
<embed src="helloworld.swf" />

[https://www.w3school.com.cn/tags/tag_embed.asp](https://www.w3school.com.cn/tags/tag_embed.asp)

<div id="fieldset"></div>
<a href="#html5elementtop">top</a>

# \<fieldset>
> define The border around the elements in the form
```html
<form>
  <fieldset>
    <legend>health information</legend>
    height: <input type="text" />
    weight: <input type="text" />
  </fieldset>
</form>
```
<form>
  <fieldset>
    <legend>health information</legend>
    height: <input type="text" />
    weight: <input type="text" />
  </fieldset>
</form>

[https://www.w3school.com.cn/tags/tag_fieldset.asp](https://www.w3school.com.cn/tags/tag_fieldset.asp)

<div id="figcaption"></div>
<a href="#html5elementtop">top</a>

# \<figcaption> <sup>5n</sup>
> define figure element title
```html
<figure>
  <figcaption>黄浦江上的的卢浦大桥</figcaption>
  <!-- <img src="shanghai_lupu_bridge.jpg" width="350" height="234" /> -->
</figure>
```
<figure>
  <figcaption>黄浦江上的的卢浦大桥</figcaption>
  <!-- <img src="shanghai_lupu_bridge.jpg" width="350" height="234" /> -->
</figure>

[https://www.w3school.com.cn/tags/tag_figcaption.asp](https://www.w3school.com.cn/tags/tag_figcaption.asp)

<div id="figure"></div>
<a href="#html5elementtop">top</a>

# \<figure>
> define Grouping of media content and their titles
```html
<figure>
  <p>黄浦江上的的卢浦大桥</p>
  <!-- <img src="shanghai_lupu_bridge.jpg" width="350" height="234" /> -->
</figure>
```
<figure>
  <p>黄浦江上的的卢浦大桥</p>
  <!-- <img src="shanghai_lupu_bridge.jpg" width="350" height="234" /> -->
</figure>

[https://www.w3school.com.cn/tags/tag_figure.asp](https://www.w3school.com.cn/tags/tag_figure.asp)

<div id="font"></div>
<a href="#html5elementtop">top</a>

# <sup>X</sup>\<font>
> not suggest use
```html
<font size="3" color="red">This is some text!</font>
<font size="2" color="blue">This is some text!</font>
<font face="verdana" color="green">This is some text!</font>
```
<font size="3" color="red">This is some text!</font>
<font size="2" color="blue">This is some text!</font>
<font face="verdana" color="green">This is some text!</font>

[https://www.w3school.com.cn/tags/tag_font.asp](https://www.w3school.com.cn/tags/tag_font.asp)

<div id="footer"></div>
<a href="#html5elementtop">top</a>

# \<footer> <sup>5n</sup>
> define page tail
```html
<footer>
  <p>Posted by: W3School</p>
  <p>Contact information: <a href="mailto:someone@example.com">someone@example.com</a>.</p>
</footer>
```
<footer>
  <p>Posted by: W3School</p>
  <p>Contact information: <a href="mailto:someone@example.com">someone@example.com</a>.</p>
</footer>

[https://www.w3school.com.cn/tags/tag_footer.asp](https://www.w3school.com.cn/tags/tag_footer.asp)

<div id="form"></div>
<a href="#html5elementtop">top</a>

# \<form> <sup>5np</sup>
> define HTML form entered by the user
```html
<form action="form_action.asp" method="get">
  <p>First name: <input type="text" name="fname" /></p>
  <p>Last name: <input type="text" name="lname" /></p>
  <input type="submit" value="Submit" />
</form>
```
<form action="form_action.asp" method="get">
  <p>First name: <input type="text" name="fname" /></p>
  <p>Last name: <input type="text" name="lname" /></p>
  <input type="submit" value="Submit" />
</form>

[https://www.w3school.com.cn/tags/tag_form.asp](https://www.w3school.com.cn/tags/tag_form.asp)

<div id="frame"></div>
<a href="#html5elementtop">top</a>

# \<frame>
> define frame page
```html
<html>

<frameset cols="25%,50%,25%">
  <frame src="frame_a.htm" />
  <frame src="frame_b.htm" />
  <frame src="frame_c.htm" />
</frameset>

</html>
```
<frameset cols="25%,50%,25%">
  <frame src="Iptable.html" />
  <frame src="frame_b.htm" />
  <frame src="frame_c.htm" />
</frameset>

<div id="frameset"></div>
<a href="#html5elementtop">top</a>

# \<frameset>
> define frame set
```html
<frameset cols="25%,50%,25%">
  <frame src="frame_a.htm" />
  <frame src="frame_b.htm" />
  <frame src="frame_c.htm" />
</frameset>
```
<frameset cols="25%,50%,25%">
  <frame src="frame_a.htm" />
  <frame src="frame_b.htm" />
  <frame src="frame_c.htm" />
</frameset>

[https://www.w3school.com.cn/tags/tag_frameset.asp](https://www.w3school.com.cn/tags/tag_frameset.asp)

<div id="h1"></div>
<a href="#html5elementtop">top</a>

# \<h1> to <h6>
```html
<h1>这是标题 1</h1>
<h2>这是标题 2</h2>
<h3>这是标题 3</h3>
<h4>这是标题 4</h4>
<h5>这是标题 5</h5>
<h6>这是标题 6</h6>
```
<h1>这是标题 1</h1>
<h2>这是标题 2</h2>
<h3>这是标题 3</h3>
<h4>这是标题 4</h4>
<h5>这是标题 5</h5>
<h6>这是标题 6</h6>

<div id="head"></div>
<a href="#html5elementtop">top</a>

# \<head>
```html
<head>
  <title>文档的标题</title>
</head>
```

<div id="header"></div>
<a href="#html5elementtop">top</a>

# \<header> <sup>5n</sup>
> define section or page head
```html
<header>
<h1>Welcome to my homepage</h1>
<p>My name is Donald Duck</p>
</header>

<p>The rest of my home page...</p>
```
<header>
<h1>Welcome to my homepage</h1>
<p>My name is Donald Duck</p>
</header>

<p>The rest of my home page...</p>

[https://www.w3school.com.cn/tags/tag_header.asp](https://www.w3school.com.cn/tags/tag_header.asp)

<div id="hr"></div>
<a href="#html5elementtop">top</a>

# \<hr>
> define Horizontal line
```html
<h1>This is header 1</h1>
<hr />
<p>This is some text</p>
```
<h1>This is header 1</h1>
<hr />
<p>This is some text</p>

[https://www.w3school.com.cn/tags/tag_hr.asp](https://www.w3school.com.cn/tags/tag_hr.asp)

<div id="i"></div>
<a href="#html5elementtop">top</a>

# \<i>
<i> hello </i>

<div id="iframe"></div>
<a href="#html5elementtop">top</a>

# \<iframe> <sup>5np</sup>
> define Inline frame
```html
<iframe src ="/index.html" align="right">
<p>Your browser does not support iframes.</p>
</iframe>
```
<iframe src ="/index.html" align="left" height="50px">
<p>Your browser does not support iframes.</p>
</iframe>

[https://www.w3school.com.cn/tags/tag_iframe.asp](https://www.w3school.com.cn/tags/tag_iframe.asp)

<div id="img"></div>
<a href="#html5elementtop">top</a>

# \<img>
> define image
```html
<img src="/i/eg_tulip.jpg"  alt="上海鲜花港 - 郁金香" />
```
<img src="https://www.w3school.com.cn/i/eg_tulip.jpg"  alt="上海鲜花港 - 郁金香" />

[https://www.w3school.com.cn/tags/tag_img.asp](https://www.w3school.com.cn/tags/tag_img.asp)

<div id="input"></div>
<a href="#html5elementtop">top</a>

# \<input> <sup>5np</sup>
> define input control
```html
<form action="form_action.asp" method="get">
  First name: <input type="text" name="fname" />
  Last name: <input type="text" name="lname" />
  <input type="submit" value="Submit" />
</form>
```
<form action="form_action.asp" method="get">
  First name: <input type="text" name="fname" />
  Last name: <input type="text" name="lname" />
  <input type="submit" value="Submit" />
</form>

[https://www.w3school.com.cn/tags/tag_input.asp](https://www.w3school.com.cn/tags/tag_input.asp)

<div id="ins"></div>
<a href="#html5elementtop">top</a>

# \<ins>
> define text inserted
```html
a dozen is <del>20</del> <ins>12</ins> pieces
```
a dozen is <del>20</del> <ins>12</ins> pieces

[https://www.w3school.com.cn/tags/tag_ins.asp](https://www.w3school.com.cn/tags/tag_ins.asp)

<div id="kbd"></div>
<a href="#html5elementtop">top</a>

# \<kbd>
[https://www.w3school.com.cn/tags/tag_kbd.asp](https://www.w3school.com.cn/tags/tag_kbd.asp)

<div id="keygen"></div>
<a href="#html5elementtop">top</a>

# \<keygen> <sup>5n</sup>
> define encrypt form submit
```html
<form action="demo_keygen.asp" method="get">
Username: <input type="text" name="usr_name" />
Encryption: <keygen name="security" />
<input type="submit" />
</form>
```
<form action="demo_keygen.asp" method="get">
Username: <input type="text" name="usr_name" />
Encryption: <keygen name="security" />
<input type="submit" />
</form>

[https://www.w3school.com.cn/tags/tag_keygen.asp](https://www.w3school.com.cn/tags/tag_keygen.asp)

<div id="label"></div>
<a href="#html5elementtop">top</a>

# \<label> <sup>5np</sup>
> define label of input element
```html
<form>
  <label for="male">Male</label>
  <input type="radio" name="sex" id="male" />
  <br />
  <label for="female">Female</label>
  <input type="radio" name="sex" id="female" />
</form>
```
<form>
  <label for="male">Male</label>
  <input type="radio" name="sex" id="male" />
  <br />
  <label for="female">Female</label>
  <input type="radio" name="sex" id="female" />
</form>

[https://www.w3school.com.cn/tags/tag_label.asp](https://www.w3school.com.cn/tags/tag_label.asp)

<div id="legend"></div>
<a href="#html5elementtop">top</a>

# \<legend>
> define the title of the fieldset element
```html
<form>
  <fieldset>
    <legend>health information</legend>
    height: <input type="text" />
    weight: <input type="text" />
  </fieldset>
</form>
```
<form>
  <fieldset>
    <legend>health information</legend>
    height: <input type="text" />
    weight: <input type="text" />
  </fieldset>
</form>

[https://www.w3school.com.cn/tags/tag_legend.asp](https://www.w3school.com.cn/tags/tag_legend.asp)

<div id="li"></div>
<a href="#html5elementtop">top</a>

# \<li>
> define display list of project
```html
<ol>
   <li>Coffee</li>
   <li>Tea</li>
   <li>Milk</li>
</ol>

<ul>
   <li>Coffee</li>
   <li>Tea</li>
   <li>Milk</li>
</ul>
```
<ol>
   <li>Coffee</li>
   <li>Tea</li>
   <li>Milk</li>
</ol>

<ul>
   <li>Coffee</li>
   <li>Tea</li>
   <li>Milk</li>
</ul>

[https://www.w3school.com.cn/tags/tag_li.asp](https://www.w3school.com.cn/tags/tag_li.asp)

<div id="link"></div>
<a href="#html5elementtop">top</a>

# \<link> <sup>5np</sup>
> define  the contact of the document and outside resource
```html
<link rel="stylesheet" type="text/css" href="theme.css" />
```

<div id="main"></div>
<a href="#html5elementtop">top</a>

# \<main> <sup>5n</sup>
> define document only one element
```html
<!DOCTYPE html>
<html>
<body>

<main>
  <h1>Web Browsers</h1>
  <p>Google Chrome、Firefox 以及 Internet Explorer 是目前最流行的浏览器。</p>

  <article>
    <h1>Google Chrome</h1>
    <p>Google Chrome 是由 Google 开发的一款免费的开源 web 浏览器，于 2008 年发布。</p>
  </article>

  <article>
    <h1>Internet Explorer</h1>
    <p>Internet Explorer 由微软开发的一款免费的 web 浏览器，发布于 1995 年。</p>
  </article>

  <article>
    <h1>Mozilla Firefox</h1>
    <p>Firefox 是一款来自 Mozilla 的免费开源 web 浏览器，发布于 2004 年。</p>
  </article>
</main> 

</body>
</html>
```

<div id="map"></div>
<a href="#html5elementtop">top</a>

# \<map>
> define picture mapping
```html
<img src="planets.jpg" border="0" usemap="#planetmap" alt="Planets" />

<map name="planetmap" id="planetmap">
  <area shape="circle" coords="180,139,14" href ="venus.html" alt="Venus" />
  <area shape="circle" coords="129,161,10" href ="mercur.html" alt="Mercury" />
  <area shape="rect" coords="0,0,110,260" href ="sun.html" alt="Sun" />
</map>
```
<img
src="https://w3school.com.cn/i/eg_planets.jpg"
border="0" usemap="#planetmap"
alt="Planets" />

<map name="planetmap" id="planetmap">

<area
shape="circle"
coords="180,139,14"
href ="/example/html/venus.html"
target ="_blank"
alt="Venus" />

<area
shape="circle"
coords="129,161,10"
href ="/example/html/mercur.html"
target ="_blank"
alt="Mercury" />

<area
shape="rect"
coords="0,0,110,260"
href ="/example/html/sun.html"
target ="_blank"
alt="Sun" />

</map>

[https://www.w3school.com.cn/tags/tag_map.asp](https://www.w3school.com.cn/tags/tag_map.asp)

<div id="mark"></div>
<a href="#html5elementtop">top</a>

# \<mark> <sup>5n</sup>
> hightlight some text
```html
<p>Do not forget to buy <mark>milk</mark> today.</p>
```
<p>Do not forget to buy <mark>milk</mark> today.</p>

[https://www.w3school.com.cn/tags/tag_mark.asp](https://www.w3school.com.cn/tags/tag_mark.asp)

<div id="menu"></div>
<a href="#html5elementtop">top</a>

# <sup>X</sup>\<menu>
> define list or menu
```html
<menu type="toolbar">
 <li>
 <menu label="File">
 <button type="button" onclick="file_new()">New...</button>
 <button type="button" onclick="file_open()">Open...</button>
 <button type="button" onclick="file_save()">Save</button>
 </menu>
 </li>
 <li>
 <menu label="Edit">
 <button type="button" onclick="edit_cut()">Cut</button>
 <button type="button" onclick="edit_copy()">Copy</button>
 <button type="button" onclick="edit_paste()">Paste</button>
 </menu>
 </li>
</menu>
```
<menu type="toolbar">
 <li>
 <menu label="File">
 <button type="button" onclick="file_new()">New...</button>
 <button type="button" onclick="file_open()">Open...</button>
 <button type="button" onclick="file_save()">Save</button>
 </menu>
 </li>
 <li>
 <menu label="Edit">
 <button type="button" onclick="edit_cut()">Cut</button>
 <button type="button" onclick="edit_copy()">Copy</button>
 <button type="button" onclick="edit_paste()">Paste</button>
 </menu>
 </li>
</menu>

<div id="menuitem"></div>
<a href="#html5elementtop">top</a>

# <sup>X</sup> \<menuitem>
```html
<menu type="context" id="mymenu">
  <menuitem label="Refresh" onclick="window.location.reload();" icon="ico_reload.png">
  </menuitem>
  <menu label="Share on...">
    <menuitem label="Twitter" icon="ico_twitter.png"
    onclick="window.open('//twitter.com/intent/tweet?text='+window.location.href);">
    </menuitem>
    <menuitem label="Facebook" icon="ico_facebook.png"
    onclick="window.open('//facebook.com/sharer/sharer.php?u='+window.location.href);">
    </menuitem>
  </menu>
  <menuitem label="Email This Page"
  onclick="window.location='mailto:?body='+window.location.href;"></menuitem>
</menu>
```
<menu type="context" id="mymenu">
  <menuitem label="Refresh" onclick="window.location.reload();" icon="ico_reload.png">
  </menuitem>
  <menu label="Share on...">
    <menuitem label="Twitter" icon="ico_twitter.png"
    onclick="window.open('//twitter.com/intent/tweet?text='+window.location.href);">
    </menuitem>
    <menuitem label="Facebook" icon="ico_facebook.png"
    onclick="window.open('//facebook.com/sharer/sharer.php?u='+window.location.href);">
    </menuitem>
  </menu>
  <menuitem label="Email This Page"
  onclick="window.location='mailto:?body='+window.location.href;"></menuitem>
</menu>

<div id="meta"></div>
<a href="#html5elementtop">top</a>

# \<meta>
> 元素可提供有关页面的元信息（meta-information），比如针对搜索引擎和更新频度的描述和关键词
```html
<meta name="keywords" content="HTML,ASP,PHP,SQL">
<meta http-equiv="charset" content="iso-8859-1">
<meta http-equiv="expires" content="31 Dec 2008">
```

<div id="meter"></div>
<a href="#html5elementtop">top</a>

# \<meter> <sup>5n</sup>
> 使用 meter 元素来度量给定范围（gauge）内的数据
```html
<meter value="3" min="0" max="10">十分之三</meter>

<meter value="0.6">60%</meter>
```
<meter value="3" min="0" max="10">十分之三</meter>

<meter value="0.6">60%</meter>

<div id="nav"></div>
<a href="#html5elementtop">top</a>

# \<nav> <sup>5n</sup>
> define nav link
```html
<nav>
<a href="index.asp">Home</a>
<a href="html5_meter.asp">Previous</a>
<a href="html5_noscript.asp">Next</a>
</nav>
```
<nav>
<a href="index.asp">Home</a>
<a href="html5_meter.asp">Previous</a>
<a href="html5_noscript.asp">Next</a>
</nav>

<div id="noframes"></div>
<a href="#html5elementtop">top</a>

# \<noframes>
```html
<frameset cols = "25%, 25%,*">
  <noframes>
  <body>Your browser does not handle frames!</body>
  </noframes>
  <frame src ="venus.htm" />
  <frame src   ="sun.htm" />
  <frame src ="mercur.htm"   />
</frameset>
```
<frameset cols = "25%, 25%,*">
  <noframes>
  <body>Your browser does not handle frames!</body>
  </noframes>
  <frame src ="venus.htm" />
  <frame src   ="sun.htm" />
  <frame src ="mercur.htm"   />
</frameset>

<div id="noscript"></div>
<a href="#html5elementtop">top</a>

# \<noscript>
```html
<body>
  ...
  ...

  <script type="text/javascript">
    <!--
    document.write("Hello World!")
    //-->
  </script>

  <noscript>Your browser does not support JavaScript!</noscript>
  ...
  ...
</body>
```

<div id="object"></div>
<a href="#html5elementtop">top</a>

# \<object>
> define a embed object
```html
<object classid="clsid:F08DF954-8592-11D1-B16A-00C0F0283628" id="Slider1"
width="100" height="50">
  <param name="BorderStyle" value="1" />
  <param name="MousePointer" value="0" />
  <param name="Enabled" value="1" />
  <param name="Min" value="0" />
  <param name="Max" value="10" />
</object>
```

<div id="ol"></div>
<a href="#html5elementtop">top</a>

# \<ol> <sup>5np</sup>
> define a rulely list
```html
<ol>
  <li>Coffee</li>
  <li>Tea</li>
  <li>Milk</li>
</ol>
```
<ol>
  <li>Coffee</li>
  <li>Tea</li>
  <li>Milk</li>
</ol>

<div id="optgroup"></div>
<a href="#html5elementtop">top</a>

# \<optgroup>
```html
<select>
  <optgroup label="Swedish Cars">
    <option value ="volvo">Volvo</option>
    <option value ="saab">Saab</option>
  </optgroup>

  <optgroup label="German Cars">
    <option value ="mercedes">Mercedes</option>
    <option value ="audi">Audi</option>
  </optgroup>
</select>
```
<select>
  <optgroup label="Swedish Cars">
    <option value ="volvo">Volvo</option>
    <option value ="saab">Saab</option>
  </optgroup>

  <optgroup label="German Cars">
    <option value ="mercedes">Mercedes</option>
    <option value ="audi">Audi</option>
  </optgroup>
</select>

<div id="option"></div>
<a href="#html5elementtop">top</a>

# \<option>
```html
<select>
  <option value ="volvo">Volvo</option>
  <option value ="saab">Saab</option>
  <option value="opel">Opel</option>
  <option value="audi">Audi</option>
</select>
```
<select>
  <option value ="volvo">Volvo</option>
  <option value ="saab">Saab</option>
  <option value="opel">Opel</option>
  <option value="audi">Audi</option>
</select>

<div id="output"></div>
<a href="#html5elementtop">top</a>

# \<output> <sup>5n</sup>
> define different type output
```html
<form oninput="x.value=parseInt(a.value)+parseInt(b.value)">0
   <input type="range" id="a" value="50">100
   +<input type="number" id="b" value="50">
   =<output name="x" for="a b"></output>
</form>
```
<form oninput="x.value=parseInt(a.value)+parseInt(b.value)">0
   <input type="range" id="a" value="50">100
   +<input type="number" id="b" value="50">
   =<output name="x" for="a b"></output>
</form>

<div id="p"></div>
<a href="#html5elementtop">top</a>

# \<p>
> define a paragraph

```html
<p>This is some text in a very short paragraph</p>
```
<p>This is some text in a very short paragraph</p>

<div id="param"></div>
<a href="#html5elementtop">top</a>

# \<param>
```html
<object classid="clsid:F08DF954-8592-11D1-B16A-00C0F0283628" id="Slider1"
width="100" height="50">
  <param name="BorderStyle" value="1" />
  <param name="MousePointer" value="0" />
  <param name="Enabled" value="1" />
  <param name="Min" value="0" />
  <param name="Max" value="10" />
</object>
```

<div id="pre"></div>
<a href="#html5elementtop">top</a>

# \<pre>
> 可定义预格式化的文本。被包围在 pre 元素中的文本通常会保留空格和换行符
```html
<pre>
&lt;html&gt;

&lt;head&gt;
  &lt;script type=&quot;text/javascript&quot; src=&quot;loadxmldoc.js&quot;&gt;
&lt;/script&gt;
&lt;/head&gt;

&lt;body&gt;

  &lt;script type=&quot;text/javascript&quot;&gt;
    xmlDoc=<a href="dom_loadxmldoc.asp">loadXMLDoc</a>(&quot;books.xml&quot;);
    document.write(&quot;xmlDoc is loaded, ready for use&quot;);
  &lt;/script&gt;

&lt;/body&gt;

&lt;/html&gt;
</pre>
```
<pre>
&lt;html&gt;

&lt;head&gt;
  &lt;script type=&quot;text/javascript&quot; src=&quot;loadxmldoc.js&quot;&gt;
&lt;/script&gt;
&lt;/head&gt;

&lt;body&gt;

  &lt;script type=&quot;text/javascript&quot;&gt;
    xmlDoc=<a href="dom_loadxmldoc.asp">loadXMLDoc</a>(&quot;books.xml&quot;);
    document.write(&quot;xmlDoc is loaded, ready for use&quot;);
  &lt;/script&gt;

&lt;/body&gt;

&lt;/html&gt;
</pre>

<div id="progress"></div>
<a href="#html5elementtop">top</a>

# \<progress> <sup>5n</sup>
```html
<progress value="22" max="100"></progress>
```
<progress value="22" max="100"></progress>

<div id="q"></div>
<a href="#html5elementtop">top</a>

# \<q>
```html
<q>Here is a short quotation here is a short quotation</q>
```
<q>Here is a short quotation here is a short quotation</q>

<div id="rp"></div>
<a href="#html5elementtop">top</a>

# \<rp>  <sup>5n</sup>
```html
<ruby>
漢 <rt><rp>(</rp>ㄏㄢˋ<rp>)</rp></rt>
</ruby>
```
<ruby>
漢 <rt><rp>(</rp>ㄏㄢˋ<rp>)</rp></rt>
</ruby>

<div id="s"></div>
<a href="#html5elementtop">top</a>

# <sup>X</sup>\<s>
```html
<s> no support</s>
```
<s> no support</s>

<div id="samp"></div>
<a href="#html5elementtop">top</a>

# \<samp>
> define computer code
```html
<samp>printf("hello world!!")</samp>
```
<samp>printf("hello world!!")</samp>

<div id="script"></div>
<a href="#html5elementtop">top</a>

# \<script> <sup>5np</sup>
```html
<script type="text/javascript">
document.write("Hello World!")
</script>
```
<script type="text/javascript">
document.write("Hello World!")
</script>

<div id="section"></div>
<a href="#html5elementtop">top</a>

# \<section> <sup>5n</sup>
> 标签定义文档中的节（section、区段）。比如章节、页眉、页脚或文档中的其他部分。
```html
<section>
  <h1>PRC</h1>
  <p>The People's Republic of China was born in 1949...</p>
</section>
```
<section>
  <h1>PRC</h1>
  <p>The People's Republic of China was born in 1949...</p>
</section>

<div id="select"></div>
<a href="#html5elementtop">top</a>

# \<select> <sup>5np</sup>
```html
<select>
  <option value ="volvo">Volvo</option>
  <option value ="saab">Saab</option>
  <option value="opel">Opel</option>
  <option value="audi">Audi</option>
</select>
```
<select>
  <option value ="volvo">Volvo</option>
  <option value ="saab">Saab</option>
  <option value="opel">Opel</option>
  <option value="audi">Audi</option>
</select>

<div id="small"></div>
<a href="#html5elementtop">top</a>

# \<small>
```html
<small> hello </small>hello
```
<small> hello </small>hello

<div id="source"></div>
<a href="#html5elementtop">top</a>

# \<source>
```html
<audio controls>
   <source src="horse.ogg" type="audio/ogg">
   <source src="horse.mp3" type="audio/mpeg">
 Your browser does not support the audio element.
</audio>
```
<audio controls>
   <source src="https://www.w3school.com.cn/i/horse.ogg" type="audio/ogg">
   <source src="https://www.w3school.com.cn/i/horse.mp3" type="audio/mpeg">
 Your browser does not support the audio element.
</audio>

<div id="span"></div>
<a href="#html5elementtop">top</a>

# \<span>
```html
<span>hello </span>
```
<span>hello </span>

<div id="strike"></div>
<a href="#html5elementtop">top</a>

# <sup>X</sup>\<strike>

> like \<s>\</s>

<div id="strong"></div>
<a href="#html5elementtop">top</a>

# \<strong>
```html
<strong>hello</strong>
```
<strong>hello</strong>

<div id="style"></div>
<a href="#html5elementtop">top</a>

# \<style>
```html
<html>
<head>
<style type="text/css">
h1 {color:red}
p {color:blue}
</style>
</head>

<body>
<h1>Header 1</h1>
<p>A paragraph.</p>
</body>
</html>
```

<div id="sub"></div>
<a href="#html5elementtop">top</a>

# \<sub>
```html
hello<sub>hello</sub>
```
hello<sub>hello</sub>

<div id="summary"></div>
<a href="#html5elementtop">top</a>

# \<summary>
```html
<details>
<summary>HTML 5</summary>
This document teaches you everything you have to learn about HTML 5.
</details>
```
<details>
<summary>HTML 5</summary>
This document teaches you everything you have to learn about HTML 5.
</details>

<div id="sup"></div>
<a href="#html5elementtop">top</a>

# \<sup>\</sup>
```html
hello<sup>hello</sup>
```
hello<sup>hello</sup>

<div id="table"></div>
<a href="#html5elementtop">top</a>

# \<table>
```html
<table border="1">
  <tr>
    <th>Month</th>
    <th>Savings</th>
  </tr>
  <tr>
    <td>January</td>
    <td>$100</td>
  </tr>
</table>
```
<table border="1">
  <tr>
    <th>Month</th>
    <th>Savings</th>
  </tr>
  <tr>
    <td>January</td>
    <td>$100</td>
  </tr>
</table>

<div id="tbody"></div>
<a href="#html5elementtop">top</a>

# \<tbody>
```html
<table border="1">
  <thead>
    <tr>
      <th>Month</th>
      <th>Savings</th>
    </tr>
  </thead>

  <tfoot>
    <tr>
      <td>Sum</td>
      <td>$180</td>
    </tr>
  </tfoot>

  <tbody>
    <tr>
      <td>January</td>
      <td>$100</td>
    </tr>
    <tr>
      <td>February</td>
      <td>$80</td>
    </tr>
  </tbody>
</table>
```
<table border="1">
  <thead>
    <tr>
      <th>Month</th>
      <th>Savings</th>
    </tr>
  </thead>

  <tfoot>
    <tr>
      <td>Sum</td>
      <td>$180</td>
    </tr>
  </tfoot>

  <tbody>
    <tr>
      <td>January</td>
      <td>$100</td>
    </tr>
    <tr>
      <td>February</td>
      <td>$80</td>
    </tr>
  </tbody>
</table>

<div id="td"></div>
<a href="#html5elementtop">top</a>

# \<td>

```html
<table border="1">
  <tr>
    <th>Month</th>
    <th>Savings</th>
  </tr>
  <tr>
    <td>January</td>
    <td>$100</td>
  </tr>
</table>
```
<table border="1">
  <tr>
    <th>Month</th>
    <th>Savings</th>
  </tr>
  <tr>
    <td>January</td>
    <td>$100</td>
  </tr>
</table>

[https://www.w3school.com.cn/tags/index.asp](https://www.w3school.com.cn/tags/index.asp)

<div id="textarea"></div>
<a href="#html5elementtop">top</a>

# \<textarea> <sup>5np</sup>

```html
<textarea rows="3" cols="20">
在w3school，你可以找到你所需要的所有的网站建设教程。
</textarea>
```
<textarea rows="3" cols="20">
在w3school，你可以找到你所需要的所有的网站建设教程。
</textarea>

[https://www.w3school.com.cn/tags/tag_textarea.asp](https://www.w3school.com.cn/tags/tag_textarea.asp)

<div id="tfoot"></div>
<a href="#html5elementtop">top</a>

# \<tfoot>
```html
<table border="1">
  <thead>
    <tr>
      <th>Month</th>
      <th>Savings</th>
    </tr>
  </thead>

  <tfoot>
    <tr>
      <td>Sum</td>
      <td>$180</td>
    </tr>
  </tfoot>

  <tbody>
    <tr>
      <td>January</td>
      <td>$100</td>
    </tr>
    <tr>
      <td>February</td>
      <td>$80</td>
    </tr>
  </tbody>
</table>
```
<table border="1">
  <thead>
    <tr>
      <th>Month</th>
      <th>Savings</th>
    </tr>
  </thead>

  <tfoot>
    <tr>
      <td>Sum</td>
      <td>$180</td>
    </tr>
  </tfoot>

  <tbody>
    <tr>
      <td>January</td>
      <td>$100</td>
    </tr>
    <tr>
      <td>February</td>
      <td>$80</td>
    </tr>
  </tbody>
</table>

<div id="th"></div>
<a href="#html5elementtop">top</a>

# \<th>

```html
<table border="1">
  <tr>
    <th>Company</th>
    <th>Address</th>
  </tr>

  <tr>
    <td>Apple, Inc.</td>
    <td>1 Infinite Loop Cupertino, CA 95014</td>
  </tr>
</table>
```
<table border="1">
  <tr>
    <th>Company</th>
    <th>Address</th>
  </tr>

  <tr>
    <td>Apple, Inc.</td>
    <td>1 Infinite Loop Cupertino, CA 95014</td>
  </tr>
</table>

<div id="thead"></div>
<a href="#html5elementtop">top</a>

# \<thead>
```html
<table border="1">
  <thead>
    <tr>
      <th>Month</th>
      <th>Savings</th>
    </tr>
  </thead>

  <tfoot>
    <tr>
      <td>Sum</td>
      <td>$180</td>
    </tr>
  </tfoot>

  <tbody>
    <tr>
      <td>January</td>
      <td>$100</td>
    </tr>
    <tr>
      <td>February</td>
      <td>$80</td>
    </tr>
  </tbody>
</table>
```
<table border="1">
  <thead>
    <tr>
      <th>Month</th>
      <th>Savings</th>
    </tr>
  </thead>

  <tfoot>
    <tr>
      <td>Sum</td>
      <td>$180</td>
    </tr>
  </tfoot>

  <tbody>
    <tr>
      <td>January</td>
      <td>$100</td>
    </tr>
    <tr>
      <td>February</td>
      <td>$80</td>
    </tr>
  </tbody>
</table>

<div id="time"></div>
<a href="#html5elementtop">top</a>

# \<time> <sup>5n</sup>

```html
<p>我们在每天早上 <time>9:00</time> 开始营业。</p>

<p>我在 <time datetime="2008-02-14">情人节</time> 有个约会。</p>
```

<p>我们在每天早上 <time>9:00</time> 开始营业。</p>

<p>我在 <time datetime="2008-02-14">情人节</time> 有个约会。</p>

<div id="title"></div>
<a href="#html5elementtop">top</a>

# \<title>
```html
<html>

  <head>
    <title>XHTML Tag Reference</title>
  </head>

  <body>
    The content of the document......
  </body>

</html>
```

<div id="tr"></div>
<a href="#html5elementtop">top</a>

# \<tr>
```html
<table border="1">
  <tr>
    <th>Month</th>
    <th>Savings</th>
  </tr>
  <tr>
    <td>January</td>
    <td>$100</td>
  </tr>
</table>
```
<table border="1">
  <tr>
    <th>Month</th>
    <th>Savings</th>
  </tr>
  <tr>
    <td>January</td>
    <td>$100</td>
  </tr>
</table>

<div id="track"></div>
<a href="#html5elementtop">top</a>

# \<track>
> \<track> 标签为诸如 video 元素之类的媒介规定外部文本轨道。用于规定字幕文件或其他包含文本的文件，当媒介播放时，这些文件是可见的。
```html
<video width="320" height="240" controls="controls">
  <source src="forrest_gump.mp4" type="video/mp4" />
  <source src="forrest_gump.ogg" type="video/ogg" />
  <track kind="subtitles" src="subs_chi.srt" srclang="zh" label="Chinese">
  <track kind="subtitles" src="subs_eng.srt" srclang="en" label="English">
</video>
```

<div id="ul"></div>
<a href="#html5elementtop">top</a>

# \<ul>

```html
<ul>
  <li>Coffee</li>
  <li>Tea</li>
  <li>Milk</li>
</ul>
```
<ul>
  <li>Coffee</li>
  <li>Tea</li>
  <li>Milk</li>
</ul>

<div id="video"></div>
<a href="#html5elementtop">top</a>

# \<video> <sup>5n</sup>
```html
<video src="/i/movie.ogg" controls="controls">
your browser does not support the video tag
</video>
```
<video src="https://w3school.com.cn/i/movie.ogg" controls="controls">
your browser does not support the video tag
</video>

<div id="wbr"></div>
<a href="#html5elementtop">top</a>

# \<wbr>
```html
<p>
如果想学习 AJAX，那么您必须熟悉 XML<wbr>Http<wbr>Request 对象。
</p>
```
<p>
如果想学习 AJAX，那么您必须熟悉 XML<wbr>Http<wbr>Request 对象。
</p>

</details>

<div id="CSS"></div>
<a href="#top">pagetop</a>

<details open>
<summary style="font-size: 60px">CSS   <sup style="font-size:30px">(Cascading Style Sheets)</sup></summary>

<a href="#cssbase">base</a><br />
<a href="#cssstyle">style</a><br />
  * <a href="#background">background</a><br /> 
  * <a href="#text">text</a><br /> 
  * <a href="#cssfont">font</a><br /> 
  * <a href="#cssalink">alink</a><br /> 
  * <a href="#csslist">list</a><br /> 
  * <a href="#csstable">table</a><br /> 
  * <a href="#cssoutline">outline</a><br /> 
  * <a href="#cssoverflow">overflow</a><br /> 

<a href="#cssboxmodel">css box model</a><br />
  * <a href="#csspadding">padding</a><br /> 
  * <a href="#cssborder">border</a><br /> 
  * <a href="#cssmargin">margin</a><br /> 
  * <a href="#cssmargincombine">margin-combine</a><br /> 

<a href="#csspositioning">CSS Positioning</a><br />
  * <a href="#cssrelative">relative</a><br /> 
  * <a href="#cssabsolute">absolute</a><br /> 
  * <a href="#cssfixed">fixed</a><br /> 
  * <a href="#cssfloat">float</a><br /> 

<a href="#cssselector">CSS Selector</a><br />
  * <a href="#cssmuticlass">muticlass</a><br /> 
  * <a href="#cssdesselector">descendant selector</a><br /> 
  * <a href="#csssubselector">subelement selector</a><br /> 
  * <a href="#cssasselector">Adjacent sibling selector</a><br /> 
  * <a href="#cssweilei">CSS 伪类</a><br /> 
  * <a href="#cssweiyuansu">CSS 伪元素</a><br /> 

<a href="#cssupgrade">CSS Upgrade</a><br />
  * <a href="#cssmarginauto">CSS 水平对齐</a><br /> 
  * <a href="#csschicun">CSS 尺寸</a><br /> 
  * <a href="#cssfenleishuxing">Classification</a><br /> 
  * <a href="#csstouming">CSS 图像透明度</a><br /> 
  * <a href="#csscounter">CSS counter</a><br /> 
  * <a href="#csscontent">CSS content</a><br /> 

<a href="#css3">css3</a><br />
  * <a href="#css3border">border</a><br /> 
  * <a href="#css3background">backgroundr</a><br /> 
  * <a href="#css3text">text</a><br /> 
  * <a href="#css3gradients">CSS3 渐变</a><br /> 
  * <a href="#css3font">font</a><br /> 
  * <a href="#css32dtransform">2D transform</a><br /> 
  * <a href="#css33dtransform">3D transform</a><br /> 
  * <a href="#css3transation">transation</a><br /> 
  * <a href="#css3animation">animation</a><br /> 
  * <a href="#css3columncount">columncounter</a><br /> 
  * <a href="#css3newusershow">CSS3 new用户界面</a><br /> 
  * <a href="#css3flexbox">flex</a><br />
  * <a href="#css3grid">grid</a><br /> 
  * <a href="#css3media">@media</a><br /> 
  

<div id="cssbase"></div>
<a href="#top">pagetop</a>

<details open>
<summary style="font-size: 30px">Base</summary>

## right the way
1. 浏览器缺省设置
2. 外部样式表
3. 内部样式表（位于 <head> 标签内部）
4. 内联样式（在 HTML 元素内部）

## selector grouping
```css
h1,h2,h3,h4,h5,h6 {
  color: green;
  }
```

## extend
```css
body {
     font-family: Verdana, sans-serif;
     }
```

## 上下文选择器 (contextual selectors)
```css
li strong {
    font-style: italic;
    font-weight: normal;
  }
```

## id selector (unique on html document)
```html
#red {color:red;}
#green {color:green;}

<p id="red">这个段落是红色。</p>
<p id="green">这个段落是绿色。</p>
```
## CSS 类选择器
```css
.center {text-align: center}
```

## CSS 属性选择器
<table class="dataintable">
<tbody><tr>
<th style="width:30%;">选择器</th>
<th>描述</th>
</tr>

<tr>
<td><a href="https://www.w3school.com.cn/cssref/selector_attribute.asp" title="CSS [attribute] 选择器">[<i>attribute</i>]</a></td>
<td>用于选取带有指定属性的元素。</td>
</tr>

<tr>
<td><a href="https://www.w3school.com.cn/cssref/selector_attribute_value.asp" title="CSS [attribute=value] 选择器">[<i>attribute</i>=<i>value</i>]</a></td>
<td>用于选取带有指定属性和值的元素。</td>
</tr>

<tr>
<td><a href="https://www.w3school.com.cn/cssref/selector_attribute_value_contain.asp" title="CSS [attribute~=value] 选择器">[<i>attribute</i>~=<i>value</i>]</a></td>
<td>用于选取属性值中包含指定词汇的元素。</td>
</tr>

<tr>
<td><a href="https://www.w3school.com.cn/cssref/selector_attribute_value_start.asp" title="CSS [attribute|=value] 选择器">[<i>attribute</i>|=<i>value</i>]</a></td>
<td>用于选取带有以指定值开头的属性值的元素，该值必须是整个单词。</td>
</tr>

<tr>
<td><a href="https://www.w3school.com.cn/cssref/selector_attr_begin.asp" title="CSS [attribute^=value] 选择器">[<i>attribute</i>^=<i>value</i>]</a></td>
<td>匹配属性值以指定值开头的每个元素。</td>
</tr>

<tr>
<td><a href="https://www.w3school.com.cn/cssref/selector_attr_end.asp" title="CSS [attribute$=value] 选择器">[<i>attribute</i>$=<i>value</i>]</a></td>
<td>匹配属性值以指定值结尾的每个元素。</td>
</tr>

<tr>
<td><a href="https://www.w3school.com.cn/cssref/selector_attr_contain.asp" title="CSS [attribute*=value] 选择器">[<i>attribute</i>*=<i>value</i>]</a></td>
<td>匹配属性值中包含指定值的每个元素。</td>
</tr>
</tbody></table>

## create css file link
### 外部样式表
```html
<head>
<link rel="stylesheet" type="text/css" href="mystyle.css" />
</head>
```
### 内部样式表
```html
<head>
<style type="text/css">
  hr {color: sienna;}
  p {margin-left: 20px;}
  body {background-image: url("images/back40.gif");}
</style>
</head>
```
### 内联样式
```html
<p style="color: sienna; margin-left: 20px">
This is a paragraph
</p>
```

</details>

<div id="cssstyle"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

<details open>
<summary style="font-size:30px">Style</summary>

<div id="background"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

### background 

1. background-color: red; [#888888, #444, rgb(255, 222, 222)]

```html
<div style="width: 50px;height: 50px;background-color: red;"></div>
```
<div style="width: 50px;height: 50px;background-color: red;"></div>

2. background-image: url('')

```html
<div style="width:300px;height:300px;background-image: url('https://avatars0.githubusercontent.com/u/57088477?s=300&v=4');"></div>
```

<div style="width:300px;height:300px;background-image: url('https://avatars0.githubusercontent.com/u/57088477?s=300&v=4');"></div>

3. background-repeat: repeat-y; [repeat-x, no-repeat]

```html
<div style="width:300px;height:300px;background-image: url('https://avatars0.githubusercontent.com/u/57088477?s=88&v=4');background-repeat: repeat-y;"></div>
```

<div style="width:300px;height:300px;background-image: url('https://avatars0.githubusercontent.com/u/57088477?s=88&v=4');background-repeat: repeat-y;"></div>

4. background-position: center; [top, bottom, right, left] or [50% 50%] or [50px 50px] or [250px 50%]

```html
<div style="width:300px;height:300px;background-image: url('https://avatars0.githubusercontent.com/u/57088477?s=88&v=4');border: 1px solid black;background-repeat: no-repeat; background-position: 250px 50%"></div>
```
<div style="width:300px;height:300px;background-image: url('https://avatars0.githubusercontent.com/u/57088477?s=88&v=4');border: 1px solid black;background-repeat: no-repeat; background-position: 250px 50%"></div>

> 1. 长度值解释的是元素内边距区左上角的偏移。偏移点是图像的左上角  
> 2. 如果图像位于 0% 0%，其左上角将放在元素内边距区的左上角。如果图像位置是 100% 100%，会使图像的右下角放在右边距的右下角

5. background-attachment: scrool; [fixed]

```html
<div style="width:300px;height:300px;border: 1px solid black;overflow: scroll"><div style="width: 200px;height: 400px;font-size: 100px;background-image: url('https://avatars0.githubusercontent.com/u/57088477?s=88&v=4');background-repeat: no-repeat;background-attachment: scroll">1</div></div>
```
> fixme: background-attachment: fixed; the background image will dispear

<div style="width:300px;height:300px;border: 1px solid black;overflow: scroll"><div style="width: 200px;height: 400px;font-size: 100px;background-image: url('https://avatars0.githubusercontent.com/u/57088477?s=88&v=4');background-repeat: no-repeat;background-attachment: scroll;">1</div></div>

> background-attachment 属性的默认值是 scroll，也就是说，在默认的情况下，背景会随文档滚动。

 6. background: #ff0000 url(/i/eg_bg_03.gif) no-repeat fixed center; 

```html
<div style="width:300px;height:300px;border: 1px solid black;background: red url('https://avatars0.githubusercontent.com/u/57088477?s=88&v=4') no-repeat center"></div>
```
<div style="width:300px;height:300px;border: 1px solid black;background: red url('https://avatars0.githubusercontent.com/u/57088477?s=88&v=4') no-repeat scroll center"></div>

<div id="csstext"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

### Text

<table class="dataintable">
  <tbody><tr>
    <th>属性</th>
    <th>描述</th>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_text_color.asp">color</a></td>
    <td>设置文本颜色</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_text_direction.asp" title="CSS direction 属性">direction</a></td>
    <td>设置文本方向。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_dim_line-height.asp">line-height</a></td>
    <td>设置行高。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_text_letter-spacing.asp" title="CSS letter-spacing 属性">letter-spacing</a></td>
    <td>设置字符间距。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_text_text-align.asp" title="CSS text-align 属性">text-align</a></td>
    <td>对齐元素中的文本。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_text_text-decoration.asp">text-decoration</a></td>
    <td>向文本添加修饰。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_text_text-indent.asp" title="CSS text-indent 属性">text-indent</a></td>
    <td>缩进元素中文本的首行。</td>
  </tr>
  <tr>
    <td>text-shadow</td>
    <td>设置文本阴影。CSS2 包含该属性，但是 CSS2.1 没有保留该属性。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_text_text-transform.asp">text-transform</a></td>
    <td>控制元素中的字母。</td>
  </tr>
  <tr>
    <td>unicode-bidi</td>
    <td>设置文本方向。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_text_white-space.asp" title="CSS white-space 属性">white-space</a></td>
    <td>设置元素中空白的处理方式。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_text_word-spacing.asp" title="CSS word-spacing 属性">word-spacing</a></td>
    <td>设置字间距。</td>
  </tr>
</tbody></table>

<div id="cssfont"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

### Font

1. 指定字体系列 font-family
```html
<div style="font-family: Times, TimesNR, 'New Century Schoolbook',
     Georgia, 'New York', serif">hello world!!</div>
```
<div style="font-family: Times, TimesNR, 'New Century Schoolbook',
     Georgia, 'New York', serif">hello world!!</div>

2. 字体风格 font-style: italic;(斜体) [normal, oblique]
```html
<div style="font-style: italic">hello world!!</div>
```

<div style="font-style: italic">hello world!!</div>

3. 字体变形 font-variant: normal; [small-caps]
```html
<div style="font-variant: small-caps">hello world!!</div>
```

<div style="font-variant: small-caps">hello world!!</div>

4. 字体加粗 font-weight: bold [100-900, normal]
```html
<div style="font-weight: 900">hello world!!</div>
```
<div style="font-weight: 900">hello world!!</div>

5. 字体大小 font-size [px or em or rem]

> this website already set font-size: 62.5% on root element
```html
<div style="font-size: 20px;border: 1px solid black">
  <div style="font-size: 20px">
    hello world!!(20px)
  </div>
  <div style="font-size: 1em">
    hello world!!(1em)
  </div>
  <div style="font-size:2em">
    hello world!!(2em)
  </div>
  <div style="font-size: 16px">
    hello world!!(16px)
  </div>
  <div style="font-size: 1.6rem">
    hello world!!(1.6rem)
  </div>
</div>
```
<div style="font-size: 20px;border: 1px solid black">
  <div style="font-size: 20px">
    hello world!!(20px)
  </div>
  <div style="font-size: 1em">
    hello world!!(1em)
  </div>
  <div style="font-size:2em">
    hello world!!(2em)
  </div>
  <div style="font-size: 16px">
    hello world!!(16px)
  </div>
  <div style="font-size: 1.6rem">
    hello world!!(1.6rem)
  </div>
</div>

<div id="cssalink"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

### a link

```html
a:link {color:#FF0000;}		/* 未被访问的链接 */
a:visited {color:#00FF00;}	/* 已被访问的链接 */
a:hover {color:#FF00FF;}	/* 鼠标指针移动到链接上 */
a:active {color:#0000FF;}	/* 正在被点击的链接 */
```

<div id="csslist"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

### list

1. 列表类型 list-style-type: square; [square, decimal, ...] https://www.w3school.com.cn/cssref/pr_list-style-type.asp
```html
<ul style="list-style-type: decimal">
<li>hello</li>
<li>world</li>
</ul>
```
<ul style="list-style-type: decimal">
<li>hello</li>
<li>world</li>
</ul>

2. 列表项图像 list-style-image: url(xxx.gif)
```html
<ul style="list-style-type: devimal;list-style-image: url('https://www.w3school.com.cn/i/eg_arrow.gif')">
<li>hello</li>
<li>world</li>
</ul>
```
<ul style="list-style-type: devimal;list-style-image: url('https://www.w3school.com.cn/i/eg_arrow.gif')">
<li>hello</li>
<li>world</li>
</ul>

3. 列表标志位置 list-style-position: inside [outside]
```html
<ul style="list-style-type: devimal;list-style-image: url('https://www.w3school.com.cn/i/eg_arrow.gif');list-style-position: inside">
<li>hello</li>
<li>world</li>
</ul>
```
<ul style="list-style-type: devimal;list-style-image: url('https://www.w3school.com.cn/i/eg_arrow.gif');list-style-position: inside">
<li>hello</li>
<li>world</li>
</ul>

4. 简写列表样式
```html
<ul style="list-style:url('https://www.w3school.com.cn/i/eg_arrow.gif') inside">
<li>hello</li>
<li>world</li>
</ul>
```

<ul style="list-style:url('https://www.w3school.com.cn/i/eg_arrow.gif') inside">
<li>hello</li>
<li>world</li>
</ul>

<div id="csstable"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

### CSS table

1. 表格边框
```html
<table style="border: 1px solid blue">
<tr>
<th style="border: 1px solid blue">0</th><th>1</th>
</tr>
<tr>
<td style="border: 1px solid blue">2</td><td>3</td>
</tr>
</table>
```
<table style="border: 1px solid blue">
<tr>
<th style="border: 1px solid blue">0</th><th>1</th>
</tr>
<tr>
<td style="border: 1px solid blue">2</td><td>3</td>
</tr>
</table>

2. 折叠边框 border-collapse: collapse
```html
<table style="border: 1px solid blue;border-collapse: collapse">
<tr>
<th style="border: 1px solid blue">0</th><th>1</th>
</tr>
<tr>
<td style="border: 1px solid blue">2</td><td>3</td>
</tr>
</table>
```
<table style="border: 1px solid blue;border-collapse: collapse">
<tr>
<th style="border: 1px solid blue">0</th><th>1</th>
</tr>
<tr>
<td style="border: 1px solid blue">2</td><td>3</td>
</tr>
</table>
3. 表格文本对齐 text-align/ vertical-align(垂直对齐方式 top or bottom)

<div id="cssoutline"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

### CSS outline : color style width
```html
<p style="border: red solid thin;outline: #00ff00 dotted thick">hello world!!</p>
```
<p style="border: red solid thin;outline: #00ff00 dotted thick">hello world!!</p>

<div id="cssoverflow"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

### CSS Overflow
```html
<div style="background-color: green;color: white;width: 85%;height: 100px;overflow: scroll">
<p>这里的文本内容是可以滚动的，滚动条方向是垂直方向。</p>
<p>这里的文本内容是可以滚动的，滚动条方向是垂直方向。</p>
<p>这里的文本内容是可以滚动的，滚动条方向是垂直方向。</p>
<p>这里的文本内容是可以滚动的，滚动条方向是垂直方向。</p>
<p>这里的文本内容是可以滚动的，滚动条方向是垂直方向。</p>
<p>这里的文本内容是可以滚动的，滚动条方向是垂直方向。</p>
</div>
```
<div style="background-color: green;color: white;width: 85%;height: 100px;overflow: scroll">
<p>这里的文本内容是可以滚动的，滚动条方向是垂直方向。</p>
<p>这里的文本内容是可以滚动的，滚动条方向是垂直方向。</p>
<p>这里的文本内容是可以滚动的，滚动条方向是垂直方向。</p>
<p>这里的文本内容是可以滚动的，滚动条方向是垂直方向。</p>
<p>这里的文本内容是可以滚动的，滚动条方向是垂直方向。</p>
<p>这里的文本内容是可以滚动的，滚动条方向是垂直方向。</p>
</div>

<table class="reference">

<tbody><tr>
<th>值</th>
<th>描述</th>
</tr>

<tr>
<td>visible</td>
<td>默认值。内容不会被修剪，会呈现在元素框之外。</td>
</tr>

<tr>
<td>hidden</td>
<td>内容会被修剪，并且其余内容是不可见的。</td>
</tr>

<tr>
<td>scroll</td>
<td>内容会被修剪，但是浏览器会显示滚动条以便查看其余的内容。</td>
</tr>

<tr>
<td>auto</td>
<td>如果内容被修剪，则浏览器会显示滚动条以便查看其余的内容。</td>
</tr>

<tr>
<td>inherit</td>
<td>规定应该从父元素继承 overflow 属性的值。</td>
</tr>
</tbody></table>


</details>

<div id="cssboxmodel"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

<details open>
<summary style="font-size: 30px">CSS Box Model</summary>

### CSS 框模型概述
![](example/css3/ct_boxmodel.gif)
![](example/css3/ct_css_boxmodel_example.gif)

<div id="csspadding"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />


### CSS 内边距 padding: top right bottom left

```html
<div style="width: 150px;padding: 10px;background-color: green;"><div style="background-color: red">padding: 10px</div</div>
```
<div style="width: 150px;padding: 10px;background-color: green;"><div style="background-color: red">padding: 10px</div></div>

<div id="cssborder"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />


### CSS 边框 border: width style color

<div style="width:100px;height:100px;border: 1px solid red"></div>

<div id="cssmargin"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />


### CSS 外边距 margin: top right bottom left

```html
<div>hello</div>
<div style="margin: 0 0 0 2cm">hello world!!</div>
```

<div>hello</div>
<div style="margin: 0 0 0 2cm">hello world!!</div>

<div id="cssmargincombine"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />


### CSS 外边距合并
```html
<div style="margin-bottom: 20px">hello</div>
<div style="margin-top: 10px">world</div>

```
<div style="margin-bottom: 20px">hello</div>
<div style="margin-top: 10px">world</div>

>外边距合并指的是，当两个垂直外边距相遇时，它们将形成一个外边距。  
>合并后的外边距的高度等于两个发生合并的外边距的高度中的较大者。
</details>


<div id="csspositioning"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />


<details open>
<summary style="font-size: 30px">CSS Positioning</summary>

#### 一切皆为框

    div、h1 或 p 元素常常被称为块级元素。这意味着这些元素显示为
    一块内容，即“块框”。与之相反，span 和 strong 等元素称为“行
    内元素”，这是因为它们的内容显示在行中，即“行内框”。

    您可以使用 display 属性改变生成的框的类型。这意味着，通过将
     display 属性设置为 block，可以让行内元素（比如 <a> 元素）
     表现得像块级元素一样。还可以通过把 display 设置为 none，
     让生成的元素根本没有框。这样的话，该框及其所有内容就不再显示
     ，不占用文档中的空间。

    但是在一种情况下，即使没有进行显式定义，也会创建块级元素。这种
    情况发生在把一些文本添加到一个块级元素（比如 div）的开头。即
    使没有把这些文本定义为段落，它也会被当作段落对待：
```html
<div>
some text
<p>Some more text.</p>
</div>
```
    在这种情况下，这个框称为无名块框，因为它不与专门定义的元素相关联。
    块级元素的文本行也会发生类似的情况。假设有一个包含三行文本的段落
    。每行文本形成一个无名框。无法直接对无名块或行框应用样式，因为没
    有可以应用样式的地方（注意，行框和行内框是两个概念）。但是，这有
    助于理解在屏幕上看到的所有东西都形成某种框。

#### CSS 定位机制
    CSS 有三种基本的定位机制：普通流、浮动和绝对定位。

    除非专门指定，否则所有框都在普通流中定位。也就是说，
    普通流中的元素的位置由元素在 (X)HTML 中的位置决定。

    块级框从上到下一个接一个地排列，框之间的垂直距离是由框
    的垂直外边距计算出来。

    行内框在一行中水平布置。可以使用水平内边距、边框和
    外边距调整它们的间距。但是，垂直内边距、边框和外边
    距不影响行内框的高度。由一行形成的水平框称为行框
    （Line Box），行框的高度总是足以容纳它包含的所有行
    内框。不过，设置行高可以增加这个框的高度。

#### CSS position 属性
##### position 属性值的含义：

    1. static
    元素框正常生成。块级元素生成一个矩形框，作为文档流的一部分，行内元素则会创建一个或多个行框，置于其父元素中。
    2. relative
    元素框偏移某个距离。元素仍保持其未定位前的形状，它原本所占的空间仍保留。
    3. absolute
    元素框从文档流完全删除，并相对于其包含块定位。包含块可能是文档中的另一
    个元素或者是初始包含块。元素原先在正常文档流中所占的空间会关闭，就好像
    元素原来不存在一样。元素定位后生成一个块级框，而不论原来它在正常流中生
    成何种类型的框。
    4. fixed
    元素框的表现类似于将 position 设置为 absolute，不过其包含块是视窗本身。

<div id="cssrelative"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS 相对定位

```html
#box_relative {
  position: relative;
  left: 30px;
  top: 20px;
}
```
![](example/css3/ct_css_positioning_relative_example.gif)

> 注意，在使用相对定位时，无论是否进行移动，元素仍然占据原来的空间。因此，移动元素会导致它覆盖其它框。

<div id="cssabsolute"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS 绝对定位
```html
<div style="width: 200px;height: 100px;position: relative;left: 0;top: 0">
  <div style="width: 50px;height: 60px;background-color: red;float: left"></div>
  <div style="position: absolute;top: 10px;left: 0px;width: 50px;height: 60px;background-color: green;float: left"></div>
  <div style="width: 50px;height: 60px;background-color: blue;float: left"></div>
</div>
```
<div style="width: 200px;height: 100px;position: relative;left: 0;top: 0">
  <div style="width: 50px;height: 60px;background-color: red;float: left"></div>
  <div style="position: absolute;top: 10px;left: 0px;width: 50px;height: 60px;background-color: green;float: left"></div>
  <div style="width: 50px;height: 60px;background-color: blue;float: left"></div>
</div>

> 绝对定位的元素的位置相对于最近的已定位祖先元素，如果元素没有已定位的祖先元素，那么它的位置相对于最初的包含块。

> 相对定位是“相对于”元素在文档中的初始位置，而绝对定位是“相对于”最近的已定位祖先元素，如果不存在已定位的祖先元素，那么“相对于”最初的包含块。

<div id="cssfixed"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS 固定定位
```html
<div style="width: 100px;height: 100px;background-color: red;position: fixed; top: 100px;left: 100px"></div>
```

<div id="cssfloat"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS 浮动 float
```html
<div style="width: 200px;height: 200px;position: relative;left: 0;top: 0;border: 1px solid black">
  <div style="width: 50px;height: 60px;background-color: red;float: left"></div>
  <div style="width: 50px;height: 60px;background-color: green;float: left"></div>
  <div style="width: 50px;height: 60px;background-color: blue;float: right"></div>
  <div style="width: 50px;height: 70px;background-color: grey;clear: both"></div>
</div>
```

<div style="width: 200px;height: 200px;position: relative;left: 0;top: 0;border: 1px solid black">
  <div style="width: 50px;height: 60px;background-color: red;float: left"></div>
  <div style="width: 50px;height: 60px;background-color: green;float: left"></div>
  <div style="width: 50px;height: 60px;background-color: blue;float: right"></div>
  <div style="width: 50px;height: 70px;background-color: grey;clear: both"></div>
</div>

</details>

<div id="cssselector"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />


<details open>
<summary style="font-size: 30px">CSS Selector</summary>

<div id="cssmuticlass"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS 多类选择器
```html
<p class="important warning">
This paragraph is a very important warning.
</p>
<style>
.important {font-weight:bold;}
.warning {font-style:italic;}
.important.warning {background:silver;}
</style>
```

<div id="cssdesselector"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS 后代选择器 (descendant selector)
```html
<style>
  div.descendantselector-red {
    width: 100px;
    height: 100px;
    background-color: red;
}
div.descendantselector-green {
    width: 100px;
    height: 100px;
    background-color: green;
}
.descendantselector-red span {
    color: white
}
.descendantselector-green span {
    color: white;
}
</style>
<div>
  <div class="descendantselector-red"></div>
  <div class="descendantselector-green"></div>
</div>
```
<div>
  <div class="descendantselector-red">
    <span>hello</span>
  </div>
  <div class="descendantselector-green">
    <span>world</span>
  </div>
</div>

<div id="csssubselector"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS 子元素选择器
```html
<style>
  .childselector ul > li {
    list-style-type: decimal;
}
</style>
<div class="childselector">
  <ul>
    <li>hello</li>
    <li>world</li>
  </ul>
</div>
```
<div class="childselector">
  <ul>
    <li>hello</li>
    <li>world</li>
  </ul>
</div>

<div id="cssaselector"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## 相邻兄弟选择器（Adjacent sibling selector）

```html
<style>
.brotherselector ul > li + li{
    font-weight: bold;
}

.brotherselector ul + ol > li {
    font-weight: 900;
}
</style>
<div class="brotherselector">
<ul>
  <li>hello</li>
  <li>world</li>
</ul>
<ol>
  <li>hello</li>
  <li>world</li>
</ol>
</div>
```

<div class="brotherselector">
<ul>
  <li>hello</li>
  <li>world</li>
</ul>
<ol>
  <li>hello</li>
  <li>world</li>
</ol>
</div>

> 选择紧接在<strong>另一个元素后的元素</strong>，而且二者有相同的父元素 

<div id="cssweilei"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS 伪类

<table class="dataintable">
  <tbody><tr>
    <th style="width:30%;">属性</th>
    <th style="width:65%;">描述</th>
    <th style="width:5%;">CSS</th>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_pseudo_active.asp">:active</a></td>
    <td>向被激活的元素添加样式。</td>
    <td>1</td>
  </tr>
	<tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_pseudo_focus.asp">:focus</a></td>
    <td>向拥有键盘输入焦点的元素添加样式。</td>
    <td>2</td>
  </tr>
	<tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_pseudo_hover.asp">:hover</a></td>
    <td>当鼠标悬浮在元素上方时，向元素添加样式。</td>
    <td>1</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_pseudo_link.asp">:link</a></td>
    <td>向未被访问的链接添加样式。</td>
    <td>1</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_pseudo_visited.asp">:visited</a></td>
    <td>向已被访问的链接添加样式。</td>
    <td>1</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_pseudo_first-child.asp">:first-child</a></td>
    <td>向元素的第一个子元素添加样式。</td>
    <td>2</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_pseudo_lang.asp">:lang</a></td>
    <td>向带有指定 lang 属性的元素添加样式。</td>
    <td>2</td>
  </tr>
</tbody></table>

<div id="cssweiyuansu"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS 伪元素

<table class="dataintable">
  <tbody><tr>
    <th style="width:30%;">属性</th>
    <th style="width:65%;">描述</th>
    <th style="width:5%;">CSS</th>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_pseudo_first-letter.asp">:first-letter</a></td>
    <td>向文本的第一个字母添加特殊样式。</td>
    <td>1</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_pseudo_first-line.asp">:first-line</a></td>
    <td>向文本的首行添加特殊样式。</td>
    <td>1</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_pseudo_before.asp">:before</a></td>
    <td>在元素之前添加内容。</td>
    <td>2</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_pseudo_after.asp">:after</a></td>
    <td>在元素之后添加内容。</td>
    <td>2</td>
  </tr>
</tbody></table>

</details>


<div id="cssupgrade"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

<details open>
<summary style="font-size: 30px">CSS Upgrade</summary>

<div id="cssmarginauto"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS 水平对齐
1. margin: auto
```html
<div style="width: 200px;height: 50px;background-color: red;">
  <div style="margin: auto;width: 100px;height: 40px;background-color: green;"></div>
</div>
```
<div style="width: 200px;height: 50px;background-color: red;">
  <div style="margin: auto;width: 100px;height: 40px;background-color: green;"></div>
</div>

<div id="csschicun"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS 尺寸

<table class="dataintable">
  <tbody><tr>
    <th>属性</th>
    <th>描述</th>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_dim_height.asp">height</a></td>
    <td>设置元素的高度。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_dim_line-height.asp">line-height</a></td>
    <td>设置行高。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_dim_max-height.asp">max-height</a></td>
    <td>设置元素的最大高度。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_dim_max-width.asp">max-width</a></td>
    <td>设置元素的最大宽度。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_dim_min-height.asp">min-height</a></td>
    <td>设置元素的最小高度。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_dim_min-width.asp">min-width</a></td>
    <td>设置元素的最小宽度。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_dim_width.asp">width</a></td>
    <td>设置元素的宽度。</td>
  </tr>
</tbody></table>

<div id="cssfenleishuxing"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS 分类属性 (Classification)

<table class="dataintable">
  <tbody><tr>
    <th>属性</th>
    <th>描述</th>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_class_clear.asp" title="CSS clear 属性">clear</a></td>
    <td>设置一个元素的侧面是否允许其他的浮动元素。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_class_cursor.asp" title="CSS cursor 属性">cursor</a></td>
    <td>规定当指向某元素之上时显示的指针类型。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_class_display.asp" title="CSS display 属性">display</a></td>
    <td>设置是否及如何显示元素。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_class_float.asp" title="CSS float 属性">float</a></td>
    <td>定义元素在哪个方向浮动。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_class_position.asp" title="CSS position 属性">position</a></td>
    <td>把元素放置到一个静态的、相对的、绝对的、或固定的位置中。</td>
  </tr>
  <tr>
    <td><a href="https://www.w3school.com.cn/cssref/pr_class_visibility.asp" title="CSS visibility 属性">visibility</a></td>
    <td>设置元素是否可见或不可见。</td>
  </tr>
</tbody></table>

<div id="csstouming"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS 图像透明度
```html
<div><img style="opacity: 0.5" src="https://www.w3school.com.cn/i/tulip_peach_blossom_w_s.jpg" /></div>
```
<div><img style="opacity: 0.5" src="https://www.w3school.com.cn/i/tulip_peach_blossom_w_s.jpg" /></div>

<div id="csscounter"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS 计数器
```html
<style>
body {
    counter-reset: section;
}
#counter > h5 {
    counter-reset: subsection;
}
#counter > h5::before {
    counter-increment: section;
    content: "Section " counter(section) ".";
}
#counter > h6::before {
    counter-increment: subsection;
    content: counter(section) "." counter(subsection) ".";
}
</style>
<div id="counter">
  <h5>hello</h5>
    <h6>hello</h6>
    <h6>hello</h6>
  <h5>world</h5>
    <h6>world</h6>
    <h6>world</h6>
  <h5>!!</h5>
    <h6>!!</h6>
    <h6>!!</h6>
</div>
```

<div id="counter">
  <h5>hello</h5>
    <h6>hello</h6>
    <h6>hello</h6>
  <h5>world</h5>
    <h6>world</h6>
    <h6>world</h6>
  <h5>!!</h5>
    <h6>!!</h6>
    <h6>!!</h6>
</div>

<div id="csscontent"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS content

<table class="reference">
  <tbody><tr>
    <th align="left" width="110px;">值</th>
    <th align="left">说明</th>
   
  </tr>
  <tr>
    <td>none</td>
    <td>设置Content，如果指定成Nothing</td>
   
  </tr>
  <tr>
    <td>normal</td>
    <td>设置content，如果指定的话，正常，默认是"none"（该是nothing）</td>
  
  </tr>
  <tr>
    <td>counter</td>
    <td>设定计数器内容</td>
    
  </tr>
	<tr>
    <td>attr<i>(attribute)</i></td>
    <td>设置Content作为选择器的属性之一。</td>
   
  </tr>
	<tr>
    <td><i>string</i></td>
    <td>设置Content到你指定的文本</td>
   
  </tr>
	<tr>
    <td>open-quote</td>
    <td>设置Content是开口引号</td>
  
  </tr>
	<tr>
    <td>close-quote</td>
    <td>设置Content是闭合引号</td>
    
  </tr>
	<tr>
    <td>no-open-quote</td>
    <td>如果指定，移除内容的开始引号</td>
   
  </tr>
	<tr>
    <td>no-close-quote</td>
    <td>如果指定，移除内容的闭合引号</td>
    
  </tr>
  <tr>
    <td>url(<i>url</i>)</td>
    <td>设置某种媒体（图像，声音，视频等内容）</td>
   
  </tr>
  <tr>
    <td>inherit</td>
    <td>指定的content属性的值，应该从父元素继承</td>

  </tr>
  </tbody></table>

</details>

<div id="css3"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />


<details open>
<summary style="font-size: 40px">CSS3</summary>

<div id="css3border"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS3 边框 

1. border-radius: 25px
```html
<div style="width: 200px;height: 100px;background-color: red;border-radius: 25px"></div>
```
<div style="width: 200px;height: 100px;background-color: red;border-radius: 25px"></div>

```html
border-top-left-radius: 2em 0.5em;
border-top-right-radius: 1em 3em;
border-bottom-right-radius: 4em 0.5em;
border-bottom-left-radius: 1em 3em;
```

2. box-shadow: h-shadow v-shadow blur spread color inset; 
<table class="dataintable">
<tbody><tr>
<th style="width:25%;">值</th>
<th>描述</th>
<th style="width:8%;">测试</th>
</tr>

<tr>
<td><i>h-shadow</i></td>
<td>必需。水平阴影的位置。允许负值。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_box-shadow">测试</a></td>
</tr>

<tr>
<td><i>v-shadow</i></td>
<td>必需。垂直阴影的位置。允许负值。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_box-shadow">测试</a></td>
</tr>

<tr>
<td><i>blur</i></td>
<td>可选。模糊距离。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_box-shadow&amp;p=3">测试</a></td>
</tr>

<tr>
<td><i>spread</i></td>
<td>可选。阴影的尺寸。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_box-shadow&amp;p=7">测试</a></td>
</tr>

<tr>
<td><i>color</i></td>
<td>可选。阴影的颜色。请参阅 CSS 颜色值。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_box-shadow&amp;p=10">测试</a></td>
</tr>

<tr>
<td>inset</td>
<td>可选。将外部阴影 (outset) 改为内部阴影。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_box-shadow&amp;p=15">测试</a></td>
</tr>
</tbody></table>

```html
<div style="width: 200px;height: 100px;background-color: red;box-shadow: 10px 10px 10px #888"></div>
```
<div style="width: 200px;height: 100px;background-color: red;box-shadow: 10px 10px 10px #888"></div>


3. border-image
```html
#round {
    border-image: url(/example/css3/border.png) 30 30 round !important;
    width: 300px;
    border: 15px solid transparent;
}
<div id="round">
在这里，图片铺满整个边框。
</div>
```

<div id="round">
在这里，图片铺满整个边框。
</div>

[https://developer.mozilla.org/zh-CN/docs/Web/CSS/border-image-slice](https://developer.mozilla.org/zh-CN/docs/Web/CSS/border-image-slice)

<div id="css3background"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS3 背景

1. background-size
```html
<div style="width: 100px;height: 100px;background-image: url('https://avatars0.githubusercontent.com/u/57088477?s=50&v=4');background-repeat: no-repeat;background-size: 80px;border: 1px solid black"></div>
```
<div style="width: 100px;height: 100px;background-image: url('https://avatars0.githubusercontent.com/u/57088477?s=50&v=4');background-repeat: no-repeat;background-size: 80px;border: 1px solid black"></div>

2. background-origin : content-box or padding-box or border-box
![](https://www.runoob.com/images/background-origin.gif)
```html
<div style="width: 50px;height: 50px;padding: 50px;border: 1px solid black;background-image: url('https://www.runoob.com/try/demo_source/smiley.gif');background-repeat: no-repeat;background-position: left;background-origin: border-box">hello world!!</div>

<div style="width: 50px;height: 50px;padding: 50px;border: 1px solid black;background-image: url('https://www.runoob.com/try/demo_source/smiley.gif');background-repeat: no-repeat;background-position: left;background-origin: content-box">hello world!!</div>
```

<div style="width: 50px;height: 50px;padding: 50px;border: 1px solid black;background-image: url('https://www.runoob.com/try/demo_source/smiley.gif');background-repeat: no-repeat;background-position: left;background-origin: border-box">hello world!!</div>

<div style="width: 50px;height: 50px;padding: 50px;border: 1px solid black;background-image: url('https://www.runoob.com/try/demo_source/smiley.gif');background-repeat: no-repeat;background-position: left;background-origin: content-box">hello world!!</div>

3. background-image: url(), url()

> CSS3 允许你在元素上添加多个背景图像
```html
<div style="width: 100px;height: 100px;background: url('https://www.runoob.com/images/compatible_chrome.gif') center, url('https://www.runoob.com/try/demo_source/smiley.gif');background-repeat: no-repeat;border: 1px solid black"></div>
```

<div style="width: 100px;height: 100px;background: url('https://www.runoob.com/images/compatible_chrome.gif') center, url('https://www.runoob.com/try/demo_source/smiley.gif');background-repeat: no-repeat;border: 1px solid black"></div>

4. background-clip: padding-box or content-box

> CSS3中background-clip背景剪裁属性是从指定位置开始绘制

```html
<div style="width: 100px; height: 100px; padding: 20px; border: 1px solid black; background: red; background-clip: content-box">hello world</div>
```
<div style="width: 100px; height: 100px; padding: 20px; border: 1px solid black; background: red; background-clip: content-box">hello world</div>



<div id="css3text"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS3 文本效果

1. text-shadow
```html
<span style="text-shadow: 5px 5px 5px #FF0000;">hello world !!</span>
```
  <span style="text-shadow: 5px 5px 5px #FF0000;">hello world !!</span>

2. word-wrap
```html
<div style="">
<p style="width: 40px;height:50px;border: 1px solid black">
ffffffffff
</p>
<p style="width: 40px;height:50px;border: 1px solid black;word-wrap: break-word">
ffffffffff
</p>
</div>
```
<div style="">
<p style="width: 40px;height:50px;border: 1px solid black">
ffffffffff
</p>
<p style="width: 40px;height:50px;border: 1px solid black;word-wrap: break-word">
ffffffffff
</p>
</div>

<table class="reference">

<tbody><tr>
<th style="width:25%">属性</th>
<th>描述</th>
<th style="width:5%">CSS</th>
</tr>

<tr>
<td><a href="https://www.runoob.com/cssref/css3-pr-hanging-punctuation.html" title="CSS3 hanging-punctuation 属性">hanging-punctuation</a></td>
<td>规定标点字符是否位于线框之外。</td>
<td>3</td>
</tr>

<tr>
<td><a href="https://www.runoob.com/cssref/css3-pr-punctuation-trim.html" title="CSS3 punctuation-trim 属性">punctuation-trim</a></td>
<td>规定是否对标点字符进行修剪。</td>
<td>3</td>
</tr>

<tr>
<td><a href="https://www.runoob.com/cssref/css3-pr-text-align-last.html" target="_blank">text-align-last</a></td>
<td>设置如何对齐最后一行或紧挨着强制换行符之前的行。</td>
<td>3</td>
</tr>

<tr>
<td><a href="https://www.runoob.com/css3-pr-text-emphasis.html" target="_blank">text-emphasis</a></td>
<td>向元素的文本应用重点标记以及重点标记的前景色。</td>
<td>3</td>
</tr>

<tr>
<td><a href="https://www.runoob.com/cssref/css3-pr-text-justify.html" title="CSS3 text-justify 属性">text-justify</a></td>
<td>规定当  text-align 设置为 "justify" 时所使用的对齐方法。</td>
<td>3</td>
</tr>

<tr>
<td><a href="https://www.runoob.com/cssref/css3-pr-text-outline.html" title="CSS3 text-outline 属性">text-outline</a></td>
<td>规定文本的轮廓。</td>
<td>3</td>
</tr>

<tr>
<td><a href="https://www.runoob.com/cssref/css3-pr-text-overflow.html" title="CSS3 text-overflow 属性">text-overflow</a></td>
<td>规定当文本溢出包含元素时发生的事情。</td>
<td>3</td>
</tr>

<tr>
<td><a href="https://www.runoob.com/cssref/css3-pr-text-shadow.html" title="CSS3 text-shadow 属性">text-shadow</a></td>
<td>向文本添加阴影。</td>
<td>3</td>
</tr>

<tr>
<td><a href="https://www.runoob.com/cssref/css3-pr-text-wrap.html" title="CSS3 text-wrap 属性">text-wrap</a></td>
<td>规定文本的换行规则。</td>
<td>3</td>
</tr>

<tr>
<td><a href="https://www.runoob.com/cssref/css3-pr-word-break.html" title="CSS3 word-break 属性">word-break</a></td>
<td>规定非中日韩文本的换行规则。</td>
<td>3</td>
</tr>

<tr>
<td><a href="https://www.runoob.com/cssref/css3-pr-word-wrap.html" title="CSS3 word-wrap 属性">word-wrap</a></td>
<td>允许对长的不可分割的单词进行分割并换行到下一行。</td>
<td>3</td>
</tr>
</tbody></table>

<div id="css3gradients"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS3 渐变（Gradients）

#### 在两个或多个指定的颜色之间显示平稳的过渡

> default 从上到下
```html
<div style="background-image: linear-gradient(red, blue); width:100px;height:100px;"></div>
```

<div style="background-image: linear-gradient(red, blue); width:100px;height:100px;"></div>

> 从左到右 to right, to top, to bottom, to bottom right, to left
```html
<div style="background-image: linear-gradient(to right, red, blue);width:100px;height:100px"></div>
```
<div style="background-image: linear-gradient(to right, red, blue);width:100px;height:100px"></div>

### 使用角度 background-image: linear-gradient(angle, color-stop1, color-stop2);
> 角度是指水平线和渐变线之间的角度，逆时针方向计算。换句话说，0deg 将创建一个从下到上的渐变，90deg 将创建一个从左到右的渐变。
![](https://www.runoob.com/wp-content/uploads/2014/07/7B0CC41A-86DC-4E1B-8A69-A410E6764B91.jpg)

```html
<div style="background-image: linear-gradient(-90deg, red, blue);width:100px;height:100px"></div>
```
<div style="background-image: linear-gradient(-90deg, red, blue);width:100px;height:100px"></div>

### 使用多个颜色结点
<div style="background-image: linear-gradient(red, blue, green);width:100px;height:100px"></div>

### 使用透明度（transparent）
> 使用 rgba() 函数来定义颜色结点。rgba() 函数中的最后一个参数可以是从 0 到 1 的值，它定义了颜色的透明度：0 表示完全透明，1 表示完全不透明。
```html
<div style="background-image: linear-gradient(to right, rgba(255, 0, 0, 0), rgba(255, 0, 0, 1));width:100px;height:100px"></div>
```
<div style="background-image: linear-gradient(to right, rgba(255, 0, 0, 0), rgba(255, 0, 0, 1));width:100px;height:100px"></div>

### 重复的线性渐变
```html
<div style="background-image: repeating-linear-gradient(red, yellow 10%, green 20%);width:100px;height:100px"></div>
```
<div style="background-image: repeating-linear-gradient(red, yellow 10%, green 20%);width:100px;height:100px"></div>

### CSS3 径向渐变 background-image: radial-gradient(shape size at position, start-color, ..., last-color);
```html
<div style="background-image: radial-gradient(red, yellow, green);width:100px;height:100px"></div>
```

<div style="background-image: radial-gradient(red, yellow, green);width:100px;height:100px"></div>

```html
<div style="background-image: radial-gradient(red 5%, yellow 15%, green 60%);width:100px;height:100px"></div>
```
<div style="background-image: radial-gradient(red 5%, yellow 15%, green 60%);width:100px;height:100px"></div>

> 设置形状:shape 参数定义了形状。它可以是值 circle 或 ellipse。其中，circle 表示圆形，ellipse 表示椭圆形。默认值是 ellipse.
```html
<div style="background-image: radial-gradient(circle, red, yellow, green);width:100px;height:100px"></div>
```
<div style="background-image: radial-gradient(circle, red, yellow, green);width:100px;height:100px"></div>

### 不同尺寸大小关键字的使用
* closest-side
<div style="background-image: radial-gradient(closest-side at 60% 55%, red, yellow, black);width:100px;height:100px"></div>

* farthest-side
<div style="background-image: radial-gradient(farthest-side at 60% 55%, red, yellow, black);width:100px;height:100px"></div>

* closest-corner
<div style="background-image: radial-gradient(closest-corner at 60% 55%, red, yellow, black);width:100px;height:100px"></div>

* farthest-corner
<div style="background-image: radial-gradient(farthest-corner at 60% 55%, red, yellow, black);width:100px;height:100px"></div>

### 重复的径向渐变

<div style="background-image: repeating-radial-gradient(red, yellow 10%, green 15%);width:100px;height:100px"></div>

<div id="css3font"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS3 字体
```html
<div id="diyfont">
  <p>hello world !!</p>
  <span>hello world!!</span>
<div>
<style>
@font-face {
    font-family: runoobfont;
    src: url('/example/css3/Sansation_Light.ttf');
}

#diyfont > p {
    font-family: runoobfont;
}
</style>
```
<div id="diyfont">
  <p>hello world !!</p>
  <span>hello world!!</span>
<div>

<div id="css32dtransform"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS3 2D 转换

* rotate(angle)
```html
<div style="transform: rotate(30deg);background-color: red;width:100px;height:100px"></div>
```
<div style="transform: rotate(30deg);background-color: red;width:100px;height:100px"></div>

* tranlate(x, y)
```html
<div style="transform: translate(50px, 20px);width:100px;height:100px;background-color: red;"></div>
```
<div style="transform: translate(50px, 20px);width:100px;height:100px;background-color: red;"></div>

* scale(x, y)
> 通过 scale() 方法，元素的尺寸会增加或减少，根据给定的宽度（X 轴）和高度（Y 轴）参数
```html
<div style="transform: scale(2, 1);width:100px;height:100px;background-color: red;"></div>
```
<div style="transform: scale(2, 1);width:100px;height:100px;background-color: red;"></div>

* skew(x, y)
```html
<div style="border-left: 1px solid black;border-bottom: 1px solid black;width: 150px;height: 150px;position: relative;"><div style=";width:100px;height:100px;background-color: red;position: absolute;left: 0;bottom: 0;"></div></div>
<br />

<div style="border-left: 1px solid black;border-bottom: 1px solid black;width: 150px;height: 150px;position: relative;"><div style="transform: skew(30deg, 20deg);width:100px;height:100px;background-color: red;position: absolute;left: 0;bottom: 0;"></div></div>
```
<div style="border-left: 1px solid black;border-bottom: 1px solid black;width: 150px;height: 150px;position: relative;"><div style=";width:100px;height:100px;background-color: red;position: absolute;left: 0;bottom: 0;"></div></div>
<br />

<div style="border-left: 1px solid black;border-bottom: 1px solid black;width: 150px;height: 150px;position: relative;"><div style="transform: skew(30deg, 20deg);width:100px;height:100px;background-color: red;position: absolute;left: 0;bottom: 0;"></div></div>

> 值 skew(30deg,20deg) 围绕 X 轴把元素翻转 30 度，围绕 Y 轴翻转 20 度。
[https://developer.mozilla.org/zh-CN/docs/Web/CSS/transform-function/skew](https://developer.mozilla.org/zh-CN/docs/Web/CSS/transform-function/skew)

* matrix(a, b, c, d, e, f)
> matrix( scaleX(), skewY(), skewX(), scaleY(), translateX(), translateY() )

```html
<div style="transform: matrix(1,0.5,-0.5,0.866,0,0);background-color: red;width:100px;height:100px">1</div>
```
<div style="transform: matrix(1,0.5,-0.5,0.866,0,0);background-color: red;width:100px;height:100px">1</div>

[https://developer.mozilla.org/pt-BR/docs/Web/CSS/transform-function/matrix](https://developer.mozilla.org/pt-BR/docs/Web/CSS/transform-function/matrix)

* transform-origin

[https://www.runoob.com/cssref/css3-pr-transform-origin.html](https://www.runoob.com/cssref/css3-pr-transform-origin.html)

[https://www.w3school.com.cn/example/css3/demo_css3_transform-origin.html](https://www.w3school.com.cn/example/css3/demo_css3_transform-origin.html)

<div id="css33dtransform"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS3 3D 转换

<div id="css3transation"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS3 过渡
transation: [transition-property, transition-duration, transition-timing-function,transition-delay]
```html
.trans:hover {
    width: 300px;
    transform: rotate(720deg);
}
.trans {
    transition: width 3s linear 2s,transform 2s;
    width:100px;
    height:100px;
    background-color: red;
}
<div class="trans" style=""></div>
```
<div class="trans" style=""></div>

<div id="css3animation"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS3 动画 
> animation: name duration timing-function delay iteration-count direction;

### timing-function
<table class="dataintable">
<tbody><tr>
<th style="width:25%;">值</th>
<th>描述</th>
<th style="width:8%;">测试</th>
</tr>

<tr>
<td>linear</td>
<td>动画从头到尾的速度是相同的。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_animation-timing-function">测试</a></td>
</tr>

<tr>
<td>ease</td>
<td>默认。动画以低速开始，然后加快，在结束前变慢。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_animation-timing-function&amp;p=2">测试</a></td>
</tr>

<tr>
<td>ease-in</td>
<td>动画以低速开始。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_animation-timing-function&amp;p=3">测试</a></td>
</tr>

<tr>
<td>ease-out</td>
<td>动画以低速结束。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_animation-timing-function&amp;p=4">测试</a></td>
</tr>


<tr>
<td>ease-in-out</td>
<td>动画以低速开始和结束。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_animation-timing-function&amp;p=5">测试</a></td>
</tr>

<tr>
<td>cubic-bezier(<i>n</i>,<i>n</i>,<i>n</i>,<i>n</i>)</td>
<td>在 cubic-bezier 函数中自己的值。可能的值是从 0 到 1 的数值。</td>
<td></td>
</tr>
</tbody></table>

### iteration-count

<table class="dataintable">
<tbody><tr>
<th style="width:25%;">值</th>
<th>描述</th>
<th style="width:8%;">测试</th>
</tr>

<tr>
<td><i>n</i></td>
<td>定义动画播放次数的数值。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_animation-iteration-count">测试</a></td>
</tr>

<tr>
<td>infinite</td>
<td>规定动画应该无限次播放。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_animation-iteration-count&amp;p=4">测试</a></td>
</tr>
</tbody></table>

### direction
<table class="dataintable">
<tbody><tr>
<th style="width:25%;">值</th>
<th>描述</th>
<th style="width:8%;">测试</th>
</tr>

<tr>
<td>normal</td>
<td>默认值。动画应该正常播放。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_animation-direction">测试</a></td>
</tr>

<tr>
<td>alternate</td>
<td>动画应该轮流反向播放。</td>
<td><a target="_blank" href="/tiy/c.asp?f=css_animation-direction&amp;p=2">测试</a></td>
</tr>
</tbody></table>

```html
@keyframes mymove {
0% { width: 200px;}
100% {width: 400px;}
}
.movemove {
    animation: mymove 2s linear 1s infinite alternate;
}
<div class="movemove" style="width:100px;height:100px;background-color: red"></div>

```

<button onclick="document.getElementById('movemove').setAttribute('class','movemove')">open animation</button>
<button onclick="document.getElementById('movemove').removeAttribute('class','movemove')">close animation</button>
<div id="movemove" style="width:100px;height:100px;background-color: red"></div>

<div id="css3columncount"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS3 多列

* column-count
> CSS3 创建多列
```html
<div style="column-count: 3;">
xxx
</div>
```
<div style="column-count: 3;">
“当我年轻的时候，我梦想改变这个世界；当我成熟以后，我发现我不能够改变这个世界，我将目光缩短了些，决定只改变我的国家；当我进入暮年以后，我发现我不能够改变我们的国家，我的最后愿望仅仅是改变一下我的家庭，但是，这也不可能。当我现在躺在床上，行将就木时，我突然意识到：如果一开始我仅仅去改变我自己，然后，我可能改变我的家庭；在家人的帮助和鼓励下，我可能为国家做一些事情；然后，谁知道呢?我甚至可能改变这个世界。”
</div>

* column-gap
> CSS3 多列中列与列间的间隙
```html
<div style="column-gap: 10px;column-count: 3;">
xxx
</div>
```
<div style="column-gap: 10px;column-count: 3;">
“当我年轻的时候，我梦想改变这个世界；当我成熟以后，我发现我不能够改变这个世界，我将目光缩短了些，决定只改变我的国家；当我进入暮年以后，我发现我不能够改变我们的国家，我的最后愿望仅仅是改变一下我的家庭，但是，这也不可能。当我现在躺在床上，行将就木时，我突然意识到：如果一开始我仅仅去改变我自己，然后，我可能改变我的家庭；在家人的帮助和鼓励下，我可能为国家做一些事情；然后，谁知道呢?我甚至可能改变这个世界。”
</div>

* column-rule: [column-rule-width, column-rule-style, column-rule-color]
> 指定了列与列间的边框样式
```html
<div style="column-gap: 10px;column-count: 3;column-rule: 5px solid red;">
xxx
</div>
```
<div style="column-gap: 10px;column-count: 3;column-rule: 5px solid red;">
“当我年轻的时候，我梦想改变这个世界；当我成熟以后，我发现我不能够改变这个世界，我将目光缩短了些，决定只改变我的国家；当我进入暮年以后，我发现我不能够改变我们的国家，我的最后愿望仅仅是改变一下我的家庭，但是，这也不可能。当我现在躺在床上，行将就木时，我突然意识到：如果一开始我仅仅去改变我自己，然后，我可能改变我的家庭；在家人的帮助和鼓励下，我可能为国家做一些事情；然后，谁知道呢?我甚至可能改变这个世界。”
</div>

### CSS3 多列属性
<br />
<table class="reference">
  <tbody><tr>
    <th style="width:28%">属性</th>
    <th>描述</th>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-column-count.html">column-count</a></td>
    <td>指定元素应该被分割的列数。</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-column-fill.html">column-fill</a></td>
    <td>指定如何填充列</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-column-gap.html">column-gap</a></td>
    <td>指定列与列之间的间隙</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-column-rule.html">column-rule</a></td>
    <td>所有 column-rule-* 属性的简写</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-column-rule-color.html">column-rule-color</a></td>
    <td>指定两列间边框的颜色</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-column-rule-style.html">column-rule-style</a></td>
    <td>指定两列间边框的样式</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-column-rule-width.html">column-rule-width</a></td>
    <td>指定两列间边框的厚度</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-column-span.html">column-span</a></td>
    <td>指定元素要跨越多少列</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-column-width.html">column-width</a></td>
    <td>指定列的宽度</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-columns.html">columns</a></td>
     <td>设置 column-width 和 column-count 的简写</td>
  </tr>
</tbody></table>

<div id="css3newusershow"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

### CSS3 new用户界面

* resize: [none, both, horizontal, vertical]
> 指定一个div元素，允许用户调整大小
```html
<div style="resize: both;border: 1px solid black;overflow:auto;">
行将就木时，我突然意识到：如果一开始我仅仅去改变我自己，
</div>
```
<div style="resize: both;border: 1px solid black;overflow:auto;">
行将就木时，我突然意识到：如果一开始我仅仅去改变我自己，
</div>

* box-sizing
> box-sizing 最主要的用法是规定容器元素的最终尺寸计算方式.
  如果你创造了一个 \<div> 没有设置 box-sizing 属性为 border-box(不设置的话默认值为 content-box.)，同
  你 设置 width:100px; border:10px solid red; padding:10px; 那么最终 div 容器的实际宽度为：
  ```100px(width)+2*10px*(padding)+2*10px(border)=140px```  
  所以你会得到一个比你预期（100px）还要更大的容器，结果就是会破坏网页布局。
  
> 注意：容易 margin 的尺寸不会被计算入最终容器宽度，因为对他的定义为对这个容器的留白，但不属于容器本身。
  如果当我们定义一个容器的 ``box-sizing`` 属性为 ``border-box`` 时（表达式：``br{box-sizing:border-box}``），那么我们创建一个和上段中相同设置的 \<div> 容器时，那么他的最终宽度即为 100px, 那么它的内容部分（content）的有效宽度变成了 ``100px-2*10px-2*10px =60px``; 所以你会得到一个你预期大小的盒子容器，但是只是被压缩了内容部分尺寸而已，但是对于整体布局而言益处颇多

```html
<div style="width:200px;height:100px;background-color: red;">
<div style="width: 100px;height: 100px;background-color: green;box-sizing: border-box;border: 10px solid black;padding: 10px;overflow: scroll;">
</div>
</div>
```

<div style="width:200px;height:100px;background-color: red;">
<div style="width: 100px;height: 100px;background-color: green;box-sizing: border-box;border: 10px solid black;padding: 10px;overflow: scroll;">
当我年轻的时候，我梦想改变这个世界；当我成熟以后，我发现我不能够改变这个世界，我将目光缩短了些，决定只改变我的国家；当我进入暮年以后，我发现我不能够改变我们的国家，我的最后愿望仅仅是改变一下我的家庭，但是，这也不可能。当我现在躺在床上，行将就木时，我突然意识到：如果一开始我仅仅去改变我自己，然后，我可能改变我的家庭；在家人的帮助和鼓励下，我可能为国家做一些事情；然后，谁知道呢?我甚至可能改变这个世界
</div>
</div>




* outline-offset
> 对轮廓进行偏移，并在超出边框边缘的位置绘制轮廓。
```html
<div style="outline:1px solid red;outline-offset: 10px;border: 1px solid black;width: 100px;height: 100px"></div>
```

<div style="outline:1px solid red;outline-offset: 10px;border: 1px solid black;width: 100px;height: 100px"></div>

<div id="css3flexbox"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

### CSS3 弹性盒子
> display: flex
* flex-direction: row | row-reverse | column | column-reverse
```html
<div style="display: flex;flex-direction: row;width: 300px;height: 200px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: red;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: black;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
</div>
```
<div style="display: flex;flex-direction: row;width: 300px;height: 200px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: red;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: black;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
</div>

* justify-content: flex-start | flex-end | center | space-between | space-around
> 把弹性项沿着弹性容器的主轴线（main axis）对齐

```html
<div style="display: flex;flex-direction: row;justify-content: space-around;width: 300px;height: 200px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: red;"></div>
<div style="width: 50px;height: 50px;background-color: black;"></div>
<div style="width: 50px;height: 50px;background-color: green;"></div>
<div style="width: 50px;height: 50px;background-color: green;"></div>
</div>
```
<div style="display: flex;flex-direction: row;justify-content: space-around;width: 300px;height: 200px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: red;"></div>
<div style="width: 50px;height: 50px;background-color: black;"></div>
<div style="width: 50px;height: 50px;background-color: green;"></div>
<div style="width: 50px;height: 50px;background-color: green;"></div>
</div>

1. flex-start：
弹性项目向行头紧挨着填充。这个是默认值。第一个弹性项的main-start外边距边线被放置在该行的main-start边线，而后续弹性项依次平齐摆放。

2. flex-end：
弹性项目向行尾紧挨着填充。第一个弹性项的main-end外边距边线被放置在该行的main-end边线，而后续弹性项依次平齐摆放。

3. center：
弹性项目居中紧挨着填充。（如果剩余的自由空间是负的，则弹性项目将在两个方向上同时溢出）。

4. space-between：
弹性项目平均分布在该行上。如果剩余空间为负或者只有一个弹性项，则该值等同于flex-start。否则，第1个弹性项的外边距和行的main-start边线对齐，而最后1个弹性项的外边距和行的main-end边线对齐，然后剩余的弹性项分布在该行上，相邻项目的间隔相等。

5. space-around：
弹性项目平均分布在该行上，两边留有一半的间隔空间。如果剩余空间为负或者只有一个弹性项，则该值等同于center。否则，弹性项目沿该行分布，且彼此间隔相等（比如是20px），同时首尾两边和弹性容器之间留有一半的间隔（1/2*20px=10px）。

![](https://www.runoob.com/wp-content/uploads/2016/04/2259AD60-BD56-4865-8E35-472CEABF88B2.jpg)

* align-items: flex-start | flex-end | center | baseline | stretch
> 设置或检索弹性盒子元素在侧轴（纵轴）方向上的对齐方式。
```html
<div style="display: flex;flex-direction: column;justify-content: space-around;align-items: center;width: 300px;height: 300px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: red;"></div>
<div style="width: 50px;height: 50px;background-color: black;"></div>
<div style="width: 50px;height: 50px;background-color: green;"></div>
<div style="width: 50px;height: 50px;background-color: green;"></div>
</div>
```
<div style="display: flex;flex-direction: column;justify-content: space-around;align-items: center;width: 300px;height: 300px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: red;"></div>
<div style="width: 50px;height: 50px;background-color: black;"></div>
<div style="width: 50px;height: 50px;background-color: green;"></div>
<div style="width: 50px;height: 50px;background-color: green;"></div>
</div>

1. flex-start：弹性盒子元素的侧轴（纵轴）起始位置的边界紧靠住该行的侧轴起始边界。
2. flex-end：弹性盒子元素的侧轴（纵轴）起始位置的边界紧靠住该行的侧轴结束边界。
3. center：弹性盒子元素在该行的侧轴（纵轴）上居中放置。（如果该行的尺寸小于弹性盒子元素的尺寸，则会向两个方向溢出相同的长度）。
4. baseline：如弹性盒子元素的行内轴与侧轴为同一条，则该值与'flex-start'等效。其它情况下，该值将参与基线对齐。
5. stretch：如果指定侧轴大小的属性值为'auto'，则其值会使项目的边距盒的尺寸尽可能接近所在行的尺寸，但同时会遵照'min/max-width/height'属性的限制。


* flex-wrap: nowrap|wrap|wrap-reverse|initial|inherit;
> 指定弹性盒子的子元素换行方式

1. nowrap - 默认， 弹性容器为单行。该情况下弹性子项可能会溢出容器。
2. wrap - 弹性容器为多行。该情况下弹性子项溢出的部分会被放置到新行，子项内部会发生断行
3. wrap-reverse -反转 wrap 排列。
```html
<div style="display: flex;flex-direction: row;flex-wrap: wrap;width: 300px;height: 200px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: red;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: black;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
</div>
```
<div style="display: flex;flex-direction: row;flex-wrap: wrap;width: 300px;height: 200px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: red;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: black;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
</div>

* align-content: flex-start | flex-end | center | space-between | space-around | stretch
> 用于修改 flex-wrap 属性的行为。类似于 align-items, 但它不是设置弹性子元素的对齐，而是设置各个行的对齐。
```html
<div style="display: flex;flex-direction: row;flex-wrap: wrap;align-content:center;width: 300px;height: 200px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: red;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: black;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
</div>
```
<div style="display: flex;flex-direction: row;flex-wrap: wrap;align-content:space-around;width: 300px;height: 200px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: red;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: black;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;"></div>
</div>


### 弹性子元素属性

* order: 1

> 用整数值来定义排列顺序，数值小的排在前面。可以为负值
```html
<div style="display: flex;flex-direction: column;justify-content: space-around;align-items: center;width: 300px;height: 300px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: red;"></div>
<div style="width: 50px;height: 50px;background-color: black;"></div>
<div style="width: 50px;height: 50px;background-color: green;"></div>
<div style="width: 50px;height: 50px;background-color: green;order: -1"></div>
</div>
```
<div style="display: flex;flex-direction: column;justify-content: space-around;align-items: center;width: 300px;height: 300px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: red;"></div>
<div style="width: 50px;height: 50px;background-color: black;"></div>
<div style="width: 50px;height: 50px;background-color: green;"></div>
<div style="width: 50px;height: 50px;background-color: green;order: -1"></div>
</div>

* 完美的居中

<div style="display: flex;width: 300px;height: 300px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: red;margin: auto;"></div>
</div>

* align-self: auto | flex-start | flex-end | center | baseline | stretch
```html
<div style="display: flex;width: 300px;height: 200px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;color: white;align-self:flex-start">flex-start</div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;color: white;align-self:flex-end">flex-end</div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;color: white;align-self:center">center</div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;color: white;align-self:baseline">baseline</div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;color: white;align-self:stretch">stretch</div>
</div>
```
<div style="display: flex;width: 300px;height: 200px;border: 1px solid black">
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;color: white;align-self:flex-start">flex-start</div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;color: white;align-self:flex-end">flex-end</div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;color: white;align-self:center">center</div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;color: white;align-self:baseline">baseline</div>
<div style="width: 50px;height: 50px;background-color: green;margin-left: 10px;color: white;align-self:stretch">stretch</div>
</div>

* flex 属性用于指定弹性子元素如何分配空间
> flex: auto | initial | none | inherit |  [ flex-grow ] || [ flex-shrink ] || [ flex-basis ]
```html
<div style="display: flex;width:200px;height: 100px;border: 1px solid black">
<div style="background-color: red;flex: 2"></div>
<div style="background-color: green;flex: 1"></div>
</div>
```
<div style="display: flex;width:200px;height: 100px;border: 1px solid black">
<div style="background-color: red;flex: 2"></div>
<div style="background-color: green;flex: 1"></div>
</div>

# display: flex
<br />
<table class="reference">
  <tbody><tr>
    <th style="width:25%">属性</th>
    <th>描述</th>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/pr-class-display.html">display</a></td>
    <td>指定 HTML  元素盒子类型。</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-flex-direction.html">flex-direction</a></td>
    <td>指定了弹性容器中子元素的排列方式</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-justify-content.html">justify-content</a></td>
    <td>设置弹性盒子元素在主轴（横轴）方向上的对齐方式。</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-align-items.html">align-items</a></td>
    <td>设置弹性盒子元素在侧轴（纵轴）方向上的对齐方式。</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-flex-wrap.html">flex-wrap</a></td>
    <td>设置弹性盒子的子元素超出父容器时是否换行。</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-align-content.html">align-content</a></td>
    <td>修改 flex-wrap 属性的行为，类似
	align-items, 但不是设置子元素对齐，而是设置行对齐</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-flex-flow.html">flex-flow</a></td>
    <td>flex-direction 和 flex-wrap 的简写</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-order.html">order</a></td>
    <td>设置弹性盒子的子元素排列顺序。</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-align-self.html">align-self</a></td>
    <td>在弹性子元素上使用。覆盖容器的 align-items 属性。</td>
  </tr>
  <tr>
    <td><a target="_blank" href="https://www.runoob.com/cssref/css3-pr-flex.html">flex</a></td>
    <td>设置弹性盒子的子元素如何分配空间。</td>
  </tr>
</tbody></table>

<div id="css3media"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS3 多媒体查询
```html
@media not|only mediatype and (expressions) {
    CSS 代码...;
}
```
```html
@media screen and (min-width: 1000px ) { 大于 1000 像素
    .mediaTest {
        width: 200px;
        height: 200px;
        background-color: red;
    }
}

@media  screen and (max-width: 1000px) { //小于 1000 像素的设备上修改背景颜色
    .mediaTest {
        width: 200px;
        height: 200px;
        background-color: black;
    }
}
<div class="mediaTest">

</div>
```

<div class="mediaTest">

</div>
<br />

### 媒体功能

<br />
<table class="reference">
  <tbody><tr>
    <th style="width:25%">值</th>
    <th>描述</th>
  </tr>  
    <tr>
    <td style="height: 31px">aspect-ratio</td>
    <td style="height: 31px">定义输出设备中的页面可见区域宽度与高度的比率</td>
    </tr>
	<tr>
    <td>color</td>
    <td>定义输出设备每一组彩色原件的个数。如果不是彩色设备，则值等于0</td>
    </tr>
	<tr>
    <td>color-index</td>
    <td>定义在输出设备的彩色查询表中的条目数。如果没有使用彩色查询表，则值等于0</td>
    </tr>
	<tr>
    <td>device-aspect-ratio</td>
    <td>定义输出设备的屏幕可见宽度与高度的比率。</td>
    </tr>
	<tr>
    <td>device-height</td>
    <td>定义输出设备的屏幕可见高度。</td>
    </tr>
	<tr>
    <td>device-width</td>
    <td>定义输出设备的屏幕可见宽度。</td>
    </tr>
	<tr>
    <td>grid</td>
    <td>用来查询输出设备是否使用栅格或点阵。</td>
    </tr>
	<tr>
    <td>height</td>
    <td>定义输出设备中的页面可见区域高度。</td>
    </tr>
	<tr>
    <td>max-aspect-ratio</td>
    <td>定义输出设备的屏幕可见宽度与高度的最大比率。</td>
    </tr>
	<tr>
    <td>max-color</td>
    <td>定义输出设备每一组彩色原件的最大个数。 </td>
    </tr>
	<tr>
    <td>max-color-index</td>
    <td>定义在输出设备的彩色查询表中的最大条目数。</td>
    </tr>
	<tr>
    <td>max-device-aspect-ratio</td>
    <td>定义输出设备的屏幕可见宽度与高度的最大比率。 </td>
    </tr>
	<tr>
    <td>max-device-height</td>
    <td>定义输出设备的屏幕可见的最大高度。</td>
    </tr>
	<tr>
    <td>max-device-width</td>
    <td>定义输出设备的屏幕最大可见宽度。</td>
    </tr>
	<tr>
    <td>max-height</td>
    <td>定义输出设备中的页面最大可见区域高度。</td>
    </tr>
	<tr>
    <td>max-monochrome</td>
    <td>定义在一个单色框架缓冲区中每像素包含的最大单色原件个数。</td>
    </tr>
	<tr>
    <td>max-resolution</td>
    <td>定义设备的最大分辨率。</td>
    </tr>
	<tr>
    <td>max-width</td>
    <td>定义输出设备中的页面最大可见区域宽度。</td>
    </tr>
	<tr>
    <td>min-aspect-ratio</td>
    <td>定义输出设备中的页面可见区域宽度与高度的最小比率。</td>
    </tr>
	<tr>
    <td>min-color</td>
    <td>定义输出设备每一组彩色原件的最小个数。</td>
    </tr>
	<tr>
    <td>min-color-index</td>
    <td>定义在输出设备的彩色查询表中的最小条目数。</td>
    </tr>
	<tr>
    <td>min-device-aspect-ratio</td>
    <td>定义输出设备的屏幕可见宽度与高度的最小比率。</td>
    </tr>
	<tr>
    <td>min-device-width</td>
    <td>定义输出设备的屏幕最小可见宽度。</td>
    </tr>
	<tr>
    <td>min-device-height</td>
    <td>定义输出设备的屏幕的最小可见高度。</td>
    </tr>
	<tr>
    <td>min-height</td>
    <td>定义输出设备中的页面最小可见区域高度。</td>
    </tr>
	<tr>
    <td>min-monochrome</td>
    <td>定义在一个单色框架缓冲区中每像素包含的最小单色原件个数</td>
    </tr>
	<tr>
    <td>min-resolution</td>
    <td>定义设备的最小分辨率。</td>
    </tr>
	<tr>
    <td>min-width</td>
    <td>定义输出设备中的页面最小可见区域宽度。</td>
    </tr>
	<tr>
    <td>monochrome</td>
    <td>定义在一个单色框架缓冲区中每像素包含的单色原件个数。如果不是单色设备，则值等于0
	</td></tr>
	<tr>
    <td>orientation</td>
    <td>定义输出设备中的页面可见区域高度是否大于或等于宽度。</td>
    </tr>
	<tr>
    <td>resolution</td>
    <td>定义设备的分辨率。如：96dpi, 300dpi, 118dpcm</td>
    </tr>
	<tr>
    <td>scan</td>
    <td>定义电视类设备的扫描工序。</td>
    </tr>
	<tr>
    <td>width</td>
    <td>定义输出设备中的页面可见区域宽度。</td>
    </tr>
</tbody>
</table>

<div id="css3grid"></div><br />
<a href="#top">pagetop</a><br />
<a href="#CSS">Css top</a><br />

## CSS3 Grid
> 注意，设为网格布局以后，容器子元素（项目）的float、display: inline-block、display: table-cell、vertical-align和column-*等设置都将失效
### display: grid
```html
div {
  display: grid;
}
```

```html
<style>
#griditem-container{
  display: grid;
  grid-template-columns: 50px 50px 50px;
  grid-template-rows: 50px 50px 50px;
}

.griditem {
  font-size: 2em;
  text-align: center;
  border: 1px solid #e5e4e9;
}

.griditem-1 {
  background-color: #ef342a;
}

.griditem-2 {
  background-color: #f68f26;
}

.griditem-3 {
  background-color: #4ba946;
}

.griditem-4 {
  background-color: #0376c2;
}

.griditem-5 {
  background-color: #c077af;
}

.griditem-6 {
  background-color: #f8d29d;
}

.griditem-7 {
  background-color: #b5a87f;
}

.griditem-8 {
  background-color: #d0e4a9;
}

.griditem-9 {
  background-color: #4dc7ec;
}
</style>
<div>
  <span>foo</span>
  <div id="griditem-container">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
  <span>bar</span>
</div>
```
<div>
  <span>foo</span>
  <div id="griditem-container">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
  <span>bar</span>
</div>


### display: inline-grid;
```html
div {
  display: inline-grid;
}
```
```html
<div>
  <span>foo</span>
  <div id="griditem-container" style="display: inline-grid">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
  <span>bar</span>
</div>
```
<div>
  <span>foo</span>
  <div id="griditem-container" style="display: inline-grid">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
  <span>bar</span>
</div>

### grid-template-columns, grid-template-rows
> 容器指定了网格布局以后，接着就要划分行和列。grid-template-columns属性定义每一列的列宽，grid-template-rows属性定义每一行的行高。
```html
<div id="griditem-container" style="grid-template-columns: 100px 100px 100px;grid-template-rows: 100px 100px 100px;">...</div>
```

<div>
  <div id="griditem-container" style="grid-template-columns: 100px 100px 100px;grid-template-rows: 100px 100px 100px;">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

> 除了使用绝对单位，也可以使用百分比

```html
<div id="griditem-container" style="grid-template-columns: 33% 33% 33%;grid-template-rows: 33% 33% 33%;">...</div>
```

<div>
  <div id="griditem-container" style="grid-template-columns: 33% 33% 33%;grid-template-rows: 33% 33% 33%;">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

1. repeat()
> 有时候，重复写同样的值非常麻烦，尤其网格很多时。这时，可以使用repeat()函数，简化重复的值

```html
<div id="griditem-container" style="grid-template-columns: repeat(3, 33%);grid-template-rows: 33% 33% 33%;">...</div>
```

<div>
  <div id="griditem-container" style="grid-template-columns: repeat(3, 33%);grid-template-rows: 33% 33% 33%;">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

> repeat()接受两个参数，第一个参数是重复的次数（上例是3），第二个参数是所要重复的值。

> repeat()重复某种模式也是可以的。

```html
<div id="griditem-container" style="grid-template-columns: repeat(2, 100px 20px 80px);grid-template-rows: 33% 33% 33%;">...</div>
```

<div>
  <div id="griditem-container" style="grid-template-columns: repeat(2, 100px 20px 80px);grid-template-rows: 33% 33% 33%;">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

> 第一列和第四列的宽度为100px，第二列和第五列为20px，第三列和第六列为80px

2. auto-fill 关键字

```html
<div id="griditem-container" style="grid-template-columns: repeat(auto-fill, 100px);grid-template-rows: 33% 33% 33%;">...</div>
```

<div>
  <div id="griditem-container" style="grid-template-columns: repeat(auto-fill, 100px);grid-template-rows: 33% 33% 33%;">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

> 每列宽度100px，然后自动填充，直到容器不能放置更多的列


3. fr 关键字

> 为了方便表示比例关系，网格布局提供了fr关键字（fraction 的缩写，意为"片段"）。如果两列的宽度分别为1fr和2fr，就表示后者是前者的两倍

```html
<div id="griditem-container" style="grid-template-columns: 1fr 2fr;grid-template-rows: 33% 33% 33%;">...</div>
```

<div>
  <div id="griditem-container" style="grid-template-columns: 1fr 2fr;grid-template-rows: 33% 33% 33%;">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

> fr可以与绝对长度的单位结合使用，这时会非常方便。

```html
<div id="griditem-container" style="grid-template-columns: 100px 1fr 2fr;grid-template-rows: 33% 33% 33%;">...</div>
```

<div>
  <div id="griditem-container" style="grid-template-columns: 100px 1fr 2fr;grid-template-rows: 33% 33% 33%;">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

4. minmax()

> minmax()函数产生一个长度范围，表示长度就在这个范围之中。它接受两个参数，分别为最小值和最大值

```html
<div id="griditem-container" style="grid-template-columns: minmax(100px, 1fr) 1fr 2fr;grid-template-rows: 33% 33% 33%;">...</div>
```

<div>
  <div id="griditem-container" style="grid-template-columns: minmax(100px, 1fr) 1fr 2fr;grid-template-rows: 33% 33% 33%;">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

> minmax(100px, 1fr)表示列宽不小于100px，不大于1fr

5. auto 关键字

```html
<div id="griditem-container" style="grid-template-columns: auto 100px 100px;grid-template-rows: 33% 33% 33%;">...</div>
```

<div>
  <div id="griditem-container" style="grid-template-columns: auto 100px 100px;grid-template-rows: 33% 33% 33%;">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

6. 网格线的名称
> grid-template-columns属性和grid-template-rows属性里面，还可以使用方括号，指定每一根网格线的名字，方便以后的引用

```html
<div id="griditem-container" style="grid-template-columns: auto 100px 100px;grid-template-rows: 33% 33% 33%;">...</div>
```

<div>
  <div id="griditem-container" style="grid-template-columns: [c1] 100px [c2] 100px [c3] 100px [c4];grid-template-rows: 33% 33% 33%;">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

> 网格布局允许同一根线有多个名字，比如[fifth-line row-5]


### row-gap 属性，column-gap 属性，gap 属性

```html
<div id="griditem-container" style="gap: 10px;grid-template-columns: auto 100px 100px;grid-template-rows: 33% 33% 33%;">...</div>
```

<div>
  <div id="griditem-container" style="gap: 10px;grid-template-columns: [c1] 100px [c2] 100px [c3] 100px [c4];grid-template-rows: 33% 33% 33%;">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

### grid-template-areas

```html
<div id="griditem-container" style="grid-template-areas: 'a b c''d e f''g h i';gap: 10px;grid-template-columns: auto 100px 100px;grid-template-rows: 33% 33% 33%;">...</div>
```

<div>
  <div id="griditem-container" style="grid-template-areas: 'a b c''d e f''g h i';gap: 10px;grid-template-columns:100px 100px  100px;grid-template-rows: 33% 33% 33%;">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

### grid-auto-flow : row | column | row dense | column dense
> 划分网格以后，容器的子元素会按照顺序，自动放置在每一个网格

* grid-auto-flow: column
```html
<div id="griditem-container" style="grid-auto-flow: column;gap: 10px;grid-template-columns: auto 100px 100px;grid-template-rows: 33% 33% 33%;">...</div>
```

<div>
  <div id="griditem-container" style="grid-auto-flow: column;gap: 10px;grid-template-columns:100px 100px  100px;grid-template-rows: 33% 33% 33%;">
    <div class="griditem griditem-1">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

* grid-auto-flow: row dense
```html
<div id="griditem-container" style="grid-auto-flow: row dense;gap: 10px;grid-template-columns: auto 100px 100px;grid-template-rows: 33% 33% 33%;">...</div>
```

<div>
  <div id="griditem-container" style="grid-auto-flow: row dense;gap: 10px;grid-template-columns:100px 100px  100px;grid-template-rows: 33% 33% 33%;">
    <div class="griditem griditem-1" style="grid-column-start: 1;grid-column-end: 3">1</div>
    <div class="griditem griditem-2" style="grid-column-start: 1;grid-column-end: 3">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>


* grid-auto-flow: column dense

```html
<div id="griditem-container" style="grid-auto-flow: column dense;gap: 10px;grid-template-columns: auto 100px 100px;grid-template-rows: 33% 33% 33%;">...</div>
```

<div>
  <div id="griditem-container" style="grid-auto-flow: column dense;gap: 10px;grid-template-columns:100px 100px 100px;grid-template-rows: 33% 33% 33%;">
    <div class="griditem griditem-1" style="grid-column-start: 1;grid-column-end: 3">1</div>
    <div class="griditem griditem-2" style="grid-column-start: 1;grid-column-end: 3">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>


### justify-items, align-items, place-items   [start | end | center | stretch]
> justify-items属性设置单元格内容的水平位置（左中右），align-items属性设置单元格内容的垂直位置（上中下）。

<div>
  <div id="griditem-container" style="justify-items: start;align-items:center;grid-auto-flow: row;grid-template-columns:100px 100px 100px;grid-template-rows: 100px 100px 100px;">
    <div class="griditem griditem-1">112</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

### justify-content, align-content，place-content
> justify-content属性是整个内容区域在容器里面的水平位置（左中右），align-content属性是整个内容区域的垂直位置（上中下）

<div>
  <div id="griditem-container" style="width:400px;height:400px;background-color: red;justify-content: center;align-content: center;grid-auto-flow: row;grid-template-columns:100px 100px 100px;grid-template-rows: 100px 100px 100px;">
    <div class="griditem griditem-1">112</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

### grid-auto-columns ,grid-auto-rows 

<div>
  <div id="griditem-container" style="width:400px;height:600px;background-color: red;justify-content: center;align-content: center;grid-auto-flow: row;grid-template-columns:100px 100px 100px;grid-template-rows: 100px 100px 100px;grid-auto-rows: 150px;">
    <div class="griditem griditem-1">112</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8" style="grid-row-start: 4;grid-column-start: 2">8</div>
    <div class="griditem griditem-9" style="grid-row-start: 5;grid-column-start: 3">9</div>
  </div>
</div>


## 项目属性

  > grid-column-start属性：左边框所在的垂直网格线  
    grid-column-end属性：右边框所在的垂直网格线  
    grid-row-start属性：上边框所在的水平网格线  
    grid-row-end属性：下边框所在的水平网格线  

<div>
  <div id="griditem-container" style="grid-auto-flow: row;grid-template-columns:100px 100px 100px;grid-template-rows: 100px 100px 100px;">
    <div class="griditem griditem-1" style="grid-column-start: 2;grid-column-end: 4">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8" style="grid-row-start: 4;grid-column-start: 2">8</div>
    <div class="griditem griditem-9" style="grid-row-start: 5;grid-column-start: 3">9</div>
  </div>
</div>

> 这四个属性的值，除了指定为第几个网格线，还可以指定为网格线的名字
```html
<div id="griditem-container" style="grid-auto-flow: row;grid-template-columns:[c1]  100px [c2] 100px [c3] 100px [c4];grid-template-rows: 100px 100px 100px;">
...
<div class="griditem griditem-1" style="grid-column-start: c1;grid-column-end: c4">1</div>
</div>
```
<div>
  <div id="griditem-container" style="grid-auto-flow: row;grid-template-columns:[c1]  100px [c2] 100px [c3] 100px [c4];grid-template-rows: 100px 100px 100px;">
    <div class="griditem griditem-1" style="grid-column-start: c1;grid-column-end: c4">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8" style="grid-row-start: 4;grid-column-start: 2">8</div>
    <div class="griditem griditem-9" style="grid-row-start: 5;grid-column-start: 3">9</div>
  </div>
</div>

这四个属性的值还可以使用``span``关键字，表示"跨越"，即左右边框（上下边框）之间跨越多少个网格。

```html
<div id="griditem-container" style="grid-auto-flow: row;grid-template-columns:[c1]  100px [c2] 100px [c3] 100px [c4];grid-template-rows: 100px 100px 100px;">
...
<div class="griditem griditem-1" style="grid-column-start: span 3">1</div>
</div>
```
<div>
  <div id="griditem-container" style="grid-auto-flow: row;grid-template-columns:[c1]  100px [c2] 100px [c3] 100px [c4];grid-template-rows: 100px 100px 100px;">
    <div class="griditem griditem-1" style="grid-column-start: span 3">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8" style="grid-row-start: 4;grid-column-start: 2">8</div>
    <div class="griditem griditem-9" style="grid-row-start: 5;grid-column-start: 3">9</div>
  </div>
</div>

>  grid-column: 1 / 3  //从第一根列线到第三根列线  
   grid-row: 1 / 2  // 占据第一行  
=  
  grid-column-start: 1;  
  grid-column-end: 3;  
  grid-row-start: 1;  
  grid-row-end: 2;  
=  
  grid-column: 1 / span 2;  
  grid-row: 1 / span 1;  

```html
<div id="griditem-container" style="grid-auto-flow: row;grid-template-columns:[c1]  100px [c2] 100px [c3] 100px [c4];grid-template-rows: 100px 100px 100px;">
...
<div class="griditem griditem-1" style="grid-column: 1 / 3;grid-row:1 / span 3">1</div>
</div>
```
<div>
  <div id="griditem-container" style="grid-auto-flow: row;grid-template-columns:[c1]  100px [c2] 100px [c3] 100px [c4];grid-template-rows: 100px 100px 100px;">
    <div class="griditem griditem-1" style="grid-column:1 / 3;grid-row:1 / span 3">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8" style="grid-row-start: 4;grid-column-start: 2">8</div>
    <div class="griditem griditem-9" style="grid-row-start: 5;grid-column-start: 3">9</div>
  </div>
</div>

> grid-area 属性指定项目放在哪一个区域。

```html
<div id="griditem-container" style="grid-auto-flow: row;grid-template-columns:[c1]  100px [c2] 100px [c3] 100px [c4];grid-template-rows: 100px 100px 100px;grid-template-areas:'a b c''d e f''g h i'">
    <div class="griditem griditem-1" style="grid-area: e;">1</div>
    ...
</div>
```
<div>
  <div id="griditem-container" style="grid-auto-flow: row;grid-template-columns:[c1]  100px [c2] 100px [c3] 100px [c4];grid-template-rows: 100px 100px 100px;grid-template-areas:'a b c''d e f''g h i'">
    <div class="griditem griditem-1" style="grid-area: e;">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>

> grid-area属性还可用作grid-row-start、grid-column-start、grid-row-end、grid-column-end的合并简写形式，直接指定项目的位置

> grid-area: \<row-start> / \<column-start> / \<row-end> / \<column-end>;
```html
<div id="griditem-container" style="grid-auto-flow: row;grid-template-columns:[c1]  100px [c2] 100px [c3] 100px [c4];grid-template-rows: 100px 100px 100px;grid-template-areas:'a b c''d e f''g h i'">
    <div class="griditem griditem-1" style="grid-area: 2 / 2 / 3 / 3;">1</div>
    ...
</div>
```
<div>
  <div id="griditem-container" style="grid-auto-flow: row;grid-template-columns:[c1]  100px [c2] 100px [c3] 100px [c4];grid-template-rows: 100px 100px 100px;grid-template-areas:'a b c''d e f''g h i'">
    <div class="griditem griditem-1" style="grid-area: 2 / 2 / 3 / 3;">1</div>
    <div class="griditem griditem-2">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>


> justify-self, align-self, place-self   

>justify-self属性设置单元格内容的水平位置（左中右），跟justify-items属性的用法完全一致，但只作用于单个项目。

>align-self属性设置单元格内容的垂直位置（上中下），跟align-items属性的用法完全一致，也是只作用于单个项目

> place-self属性是align-self属性和justify-self属性的合并简写形式


```html
<div id="griditem-container" style="grid-auto-flow: row;grid-template-columns:[c1]  100px [c2] 100px [c3] 100px [c4];grid-template-rows: 100px 100px 100px;grid-template-areas:'a b c''d e f''g h i'">
    <div class="griditem griditem-1" style="grid-area: 2 / 2 / 3 / 3;justify-self: start;align-self: center">1</div>
    <div class="griditem griditem-2" style="place-self: start start">2</div>>
    ...
</div>
```
<div>
  <div id="griditem-container" style="grid-auto-flow: row;grid-template-columns:[c1]  100px [c2] 100px [c3] 100px [c4];grid-template-rows: 100px 100px 100px;grid-template-areas:'a b c''d e f''g h i'">
    <div class="griditem griditem-1" style="grid-area: 2 / 2 / 3 / 3;justify-self: start;align-self: center">1</div>
    <div class="griditem griditem-2" style="place-self: start start">2</div>
    <div class="griditem griditem-3">3</div>
    <div class="griditem griditem-4">4</div>
    <div class="griditem griditem-5">5</div>
    <div class="griditem griditem-6">6</div>
    <div class="griditem griditem-7">7</div>
    <div class="griditem griditem-8">8</div>
    <div class="griditem griditem-9">9</div>
  </div>
</div>


</details>













</details>