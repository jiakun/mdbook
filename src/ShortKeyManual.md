# Vim  
## insert mode
    control+l     :choose snip  
    control+y     :jumpforwardtrigger course  
    control+r     :jumpbackwardtrigger couse  

    control+z     :autocompletion snip  

    F4          :Gorun  
    F1          :Run python file  
    F3          :Run C file  
    F2          :NERDTree  
    F8          :Tagbar





## normal mode
    F4          :Gorun  
    F1          :Run python file  
    F3          :Run C file  
    F2          :NERDTree  
    F8          :Tagbar  
  
    enter         :change line  
    backspace     :delete word
  
    shift + k     :5  
    ...etc  
    
    shift + n     :0  
   
    !             :F1  
    @             :F2  
    ...etc  
  
    space + w     :w!  
    space +j      :change group  
    ...etc  
  
    space + f     :pagedown  
    space + d     :pageup  
  ## visual mode
 * fitst ```shift + v```choose line, then ```ctrl +v```to choose u want to , then ```shift + i```, input ``//``, and double ``ESC``
 
 
  # Webstorm
    F2,alt+1            :Project subWindow  
    F8            :struct 
    alt + 5           :terminal  
    alt+9         :version control  
    alt+ctrl+s    :open settings  
    ctrl+tab      :open switcher  
    esc           :focus on editor  
    alt + >,<     : change file edit  
    alt + 5       :mongodb explore  
> sync by github rep
  
# System  
    alt + t         :open guaketerminal
    alt + g         : open chroium
    alt + m         :open 163 music when playing music
    alt + c         : open chrome
    alt + w         : open Webstorm when running
    shift + super + t :disable touchpad
    shift + super + y :enable touchpad
> xbindkeys -f .xbindkeysrc    
>change key mapping ```xmodmap .Xmodmap```  ```xmodmap -pm``` ```xev```

# Chrome
    r               : refresh page
    og              : open google search
    i               : focus on input element
    j               : line down
    k               : line up
    d               :half page down
    e               :half page up
    gg              : website head
    G               : website tail
    ctrl+i          :open vim edior
    E               : left page
    R                : right page
    ZZ              : save session and close browser
    ZR               : open save session 
    f                 : hint link with Abc
> surfingkeys 
