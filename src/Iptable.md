# my ubuntu rules
```shell
  iptables -t filter -I INPUT -p icmp -j DROP
```
# single server rules (means: databaseserver and backserver on a server)
```
  iptables -t filter -I OUTPUT -m state --state NEW -j DROP
```
# others

## define default policy  
```
  iptables -t filter --policy FORWARD DROP  
  iptables -t filter -P FORWARD ACCEPT  
```
## add  
```
  iptables -t filter -I INPUT -p icmp -j DROP #above all rules  
  iptables -t filter -I INPUT 3  -p icmp -j DROP # insert on three  
```
## mod 
```
  Iptables -t filter -R INPUT 2 -p icmp -j DROP  # replace second rule or update  
```
## delete
```
  iptables -t filter -D INPUT 1  
  iptables -t filter -D INPUT 3  

  iptables -t filter -F INPUT # delete all rules on filter table  
```
## match advance
```
  iptables -t filter -I INPUT -p icmp -i eth3 -j DROP # refuse icmp protocol  
  iptables  -t filter -I FORWARD -p tcp -s 192.168.80.123/32   -d 192.168.10.123/32 --dport 3389 -j DROP  
  iptables -t filter -I FORWARD -p tcp -s 192.168.80.0/24 -d 192.168.10.0/24 -j DROP   
```
## base on connect status
  -m state --state [NEW, ESTABLISHED, RELATED, INVALID]   
```
  iptables -t filter -I FORWARD -s 192.168.10.0/24 -d 192.168.80.0/24 -m state --state NEW -j DROP # on router  
  iptables -t filter -I OUTPUT -m state--state NEW -j DROP # on web server or others  
```
## limit speed
```
  iptables -t filter -I INPUT -s 180.208.59.230 -d 192.168.188.107 -j DROP 
  iptables -t filter -I INPUT -s 180.208.59.230 -d 192.168.188.107 -m limit --limit 300/second -j ACCEPT #limit speed.cn  

  iptables -t filter -I FORWARD -s 192.168.10.0/24 -d 192.168.80.0/24 -j DROP  
  iptables -t filter -I FORWARD -s 192.168.10.0/24 -d 192.168.80.0/24 -m limit --limit 300/second -j ACCEPT #300*1500/1024*8 = 3.5Mbyte  
```
## connlimit
```
  iptables -t filter -I FORWARD -s 192.168.10.0/24 -d 192.168.80.0/24 -p tcp --dport 3389 -m connlimit --connlimit-above 2 -j DROP  
```
## mac address filter
```
  iptables -t filter -I FORWARD -d 192.168.10.0/24 -m mac --mac-source xx-xx-xx-xx-xx-xx -j DROP  
```

