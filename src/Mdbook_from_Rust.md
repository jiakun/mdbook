# Where am I from

1. Install Rust
    
        open “https://www.rust-lang.org/zh-CN/tools/install” in browser, read the manual to install Rust..
        
1. Change the cargo registry source URL for update
    
        $ cd ~/.cargo/
        $ vim config
    
1. Paste blow text
    
        [source.crates-io]
        registry = "https://github.com/rust-lang/crates.io-index"
        replace-with = 'ustc'
        [source.ustc]
        registry = "git://mirrors.ustc.edu.cn/crates.io-index"
1. Exec blow command
        
        cargo install mdbook #wait a minute ,the programm will compiler source code

1. How use
        
        mdbook --help
        
        mdbook serve --open # usually
                
1. Enjoy !!    
   And notice your ```.profile``` & ```bashrc``` file, maybe you should run command```source .profile```